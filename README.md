Libs are used in this React project.

1. Formik and Yup
  I utilized the FormikForm to have built-in eventHandlers and hooks. Formik also provides an awsome validation mechanism using Yup.
  Yup allows creating a validation schema which is very fit for dynamic rendering.
2. Material-UI
  Beautify the UI
3. Lodash

In the current version, there are 3 main components including:

1. FormFieldRenderer
  This component will receive an object schema which defines how to render a field. Object key in the schema is the name of component, as well as an attibute/node in json data object. Body of an object key must include component type (text, select, radio, ...) and its props. It may have few special object like disabledIf/requiredIf for conditional rendering. After process an object key in the schema, it will pass to FormFieldArray or FormField to render component.
  Ex: question.meta.ts and answer.meta.ts

2. FormFieldArray
  This component will also accept a schema that its type is array but not object like FormFieldRenderer. Each element in the schema will be considered as a schema for FormFieldRenderer.

3. FormField
  This component will actually render a Formik Field with a custom component.
  The custom components must be registered in register-component.ts file.
  If a form field is a button with an action (onClickFn). That action must be predefined and its name must be registered in actions/event.handler.ts

Limitations:

1. Minor performance issue with Textbox. There's a bit lagging as typing. Worse if rendering too many components.
2. Having many styled components (Material-UI) in a page also cause performance issue.
3. Dependent rendering only for disabled and required (validation)
4. The schema structure must be similar to DTO from Backend.

Next steps:

1. Support more conditional rendering (in command)
2. More complex custom components (table, nested form)
3. Allow add/remove new comopnents at runtime