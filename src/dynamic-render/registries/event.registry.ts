import { FormikContextType } from "formik";
import { get } from "lodash";

let eventHandlers = {
};

export function getEventHandler(name: string, formikContext?: FormikContextType<any>, ...args: any[]) {
  const fn = get(eventHandlers, name, null);
  if(fn) {
    return fn(formikContext, ...args);
  }
  return null;
}

export function registerEvents(events: any) {
  Object.keys(eventHandlers).forEach(k => {
    if(Object.keys(events).includes(k)) {
      console.warn(`${k} is duplidated`);
    }
  });
  eventHandlers = {...eventHandlers, ...events};
}