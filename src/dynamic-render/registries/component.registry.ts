import { Image } from "antd";
import React from "react";
import { SimpleButton } from "../components/Button";
import Checkboxes from "../components/Checkboxes";
import DateTimeField from "../components/DatePicker";
import { DynamicTable } from "../components/DynamicTable";
import { HorizontalLine } from "../components/HorizontalLine";
import { Html } from "../components/Html";
import { LabelField } from "../components/Label";
import { DynamicLink } from "../components/Link";
import Loading from "../components/Loading";
import RadioButtonGroup from "../components/RadioButtonGroup";
import InputSelect from "../components/InputSelect";
import { SubTitle } from "../components/SubTitle";
import InputText from "../components/InputText";
import { PageTitle } from "../components/Title";
import InputPassword from "../components/InputPassword";
import { IconButton } from "../components/IconButton";
import MapComponent from "../components/GoogleMap";
import { Icon } from "../components/Icon";

type CustomComponentsType = { [key: string]: ((props: any) => JSX.Element) | React.FC<any> };

export const CustomComponents: CustomComponentsType = {
  text: InputText,
  button: SimpleButton,
  radio: RadioButtonGroup,
  select: InputSelect,
  title: PageTitle,
  subTitle: SubTitle,
  checkboxes: Checkboxes,
  datePicker: DateTimeField,
  html: Html,
  label: LabelField,
  loading: Loading,
  hr: HorizontalLine,
  link: DynamicLink,
  image: Image,
  table: DynamicTable,
  password: InputPassword,
  iconButton: IconButton,
  map: MapComponent,
  icon: Icon
};

export const DisplayFields = [
  'button',
  'title',
  'subTitle',
  'html',
  'label',
  'loading',
  'hr',
  'image',
  'table',
  'link',
  'iconButton',
  'map',
  'icon'
]

export function getCustomComponent(name: string) {
  return CustomComponents[name];
}

type DynamicComponent = (props: any) => JSX.Element;

export function registerComponent(
  name: string,
  component: DynamicComponent,
  onlyDisplay: boolean,
  overwrite = false
) {
  if (!overwrite) {
    if (CustomComponents[name]) {
      return; // skip register
    }
  }
  CustomComponents[name] = component;

  if (onlyDisplay) {
    DisplayFields.push(name);
  }
}

export function registerComponents(components: {name: string, component: DynamicComponent, onlyDisplay: boolean, overwrite?: boolean}[]) {
  components.forEach((component) => {
    registerComponent(component.name, component.component, component.onlyDisplay, component.overwrite);
  });
}
