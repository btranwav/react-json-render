const yup = require('yup');

yup.addMethod(yup.object, 'atLeastOneOf', function (this: any, requiredFields: string[], message: string) {
   return this.test({
    name: 'atLeastOneOf',
    message,
    exclusive: true,
    params: { keys: requiredFields.join(', ') },
    test: (value: any) => value !== null && requiredFields.some(f => value[f])
  });
});

export default yup;