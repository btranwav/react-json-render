import { FormikContextType } from "formik";
import { get } from "lodash";

let tableActions = {
};

export function getTableAction(action: string, formikContext?: FormikContextType<any>) {
  const fn = get(tableActions, action, null);
  if(fn) {
    return fn(formikContext);
  }
  return null;
}

export function registerTableActions(newActions: any) {
  tableActions = {...tableActions, ...newActions};
}