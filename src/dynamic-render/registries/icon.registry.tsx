import { UserOutlined, LockOutlined, MailOutlined, HomeOutlined, SearchOutlined, PlusCircleOutlined, ArrowLeftOutlined, LogoutOutlined, UploadOutlined, ExportOutlined } from '@ant-design/icons';

export const ICON = {
    UserOutlined: 'UserOutlined',
    LockOutlined: 'LockOutlined',
    MailOutlined: 'MailOutlined',
    HomeOutlined: 'HomeOutlined',
    SearchOutlined: 'SearchOutlined',
    PlusCircleOutlined: 'PlusCircleOutlined',
    ArrowLeftOutlined: 'ArrowLeftOutlined',
    LogoutOutlined: 'LogoutOutlined',
    UploadOutlined: 'UploadOutlined',
    ExportOutlined: 'ExportOutlined',
}

export default function getIcon(name: string) {
    switch (name) {
        case ICON.UserOutlined:
            return <UserOutlined rev={undefined} />;
        case ICON.LockOutlined:
            return <LockOutlined rev={undefined} />;
        case ICON.MailOutlined:
            return <MailOutlined rev={undefined} />;
        case ICON.HomeOutlined:
            return <HomeOutlined rev={undefined} />;
        case ICON.SearchOutlined:
            return <SearchOutlined rev={undefined} />;
        case ICON.PlusCircleOutlined:
            return <PlusCircleOutlined rev={undefined} />;
        case ICON.ArrowLeftOutlined:
            return <ArrowLeftOutlined rev={undefined} />
        case ICON.LogoutOutlined:
            return <LogoutOutlined rev={undefined} />;
        case ICON.UploadOutlined:
            return <UploadOutlined rev={undefined} />;
        case ICON.ExportOutlined:
            return <ExportOutlined rev={undefined} />;
        default:
            return undefined;
    }
}