const blackList = [
  'hiddenIf',
  'disabledIf',
  'style',
  'onlyDisplay',
  'options',
];

export function validateData(schema: any, data: any, parentName = "") {
  Object.keys(schema)
    .filter(key => {
      const fieldSchema = schema[key];
      return (typeof fieldSchema !== "string" && typeof fieldSchema !== "number" && !blackList.includes(key));
    })
    .forEach((key: string) => {
      const fieldSchema = schema[key];
      const hierrarchyName = buildHierrachyName(parentName, key);

      if (Array.isArray(fieldSchema)) {
        if (!data[key]) {
          data[key] = [{}];
          validateData(fieldSchema[0], data[key][0], hierrarchyName);
        } else {
          data[key].forEach((element: any) => {
            validateData(fieldSchema[0], element, hierrarchyName);
          });
        }

      } else {
        const { component, value } = fieldSchema;

        if (component) {
          if (!data[key]) {
            data[key] = value || getDefaultValue(component);
          }
          return;
        }

        const ignoreThisKey = key.startsWith("wrapper") || key === "group" || key === 'tabs';
        if (!ignoreThisKey && !data[key]) {
          data[key] = {};
        }
        validateData(fieldSchema, ignoreThisKey ? data : data[key], hierrarchyName);
      }
    });
}

function getDefaultValue(componentType: string) {
  switch (componentType) {
    case 'text':
    case 'radio':
    case 'select':
    case 'dynamicSelect':
      return '';
    case 'checkbox':
    case 'enhanceCheckbox':
      return false;
    case 'grades':
      return [];
    default:
      return undefined;
  }
}

export function buildHierrachyName(parentName: string, key: string) {
  const name = key.startsWith("wrapper") || key === "group" ? "" : key;

  if (parentName) {
    return parentName + (name ? "." + name : name);
  }

  return name;
}