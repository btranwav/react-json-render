import { Form, Row } from "antd";
import { Formik } from "formik";
import { FormFieldRenderer } from "./FormFieldRenderer";
import '../components/stylesheets/dynamic-render.scss';

export default function DynamicForm({ id, initialValue, formSchema, validationSchema, layout = 'vertical' }: any) {

  return (
    <div id={id}>
      <Formik
        initialValues={initialValue}
        enableReinitialize={true}
        validationSchema={validationSchema}
        validateOnChange={false}
        validateOnBlur={true}
        onSubmit={() => {
          // nothing
        }}
      >
        {() => {
          return (

            <Form layout={layout} requiredMark={true}>
              {formSchema && (
                <Row className="dynamic__form">
                  <FormFieldRenderer schema={formSchema} name={""} />
                </Row>
              )}
            </Form>
          );
        }}
      </Formik>
    </div>
  )
}