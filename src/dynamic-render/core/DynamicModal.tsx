import { Form, Modal } from "antd";
import { Formik, FormikContextType } from "formik";
import { FormFieldRenderer } from "./FormFieldRenderer";
import { useState, useEffect } from "react";

export default function DynamicModal({
  formConfig,
  onOk,
  onCancel,
  layout = "vertical",
  ...rest
}: any) {

  const [formSchema, setFormSchema] = useState(formConfig.formSchema);
  const [validationSchema, setValidationSchema] = useState(
    formConfig.validationSchema
  );
  const [initialValue, setInitialValue] = useState(formConfig.initialValue);

  useEffect(() => {
    setFormSchema(formConfig.formSchema);
    setInitialValue(formConfig.initialValue);
    setValidationSchema(formConfig.validationSchema);
    // eslint-disable-next-line
  }, [formConfig]);

  return (
    <div className="dynamic__modal">
      <Formik
        initialValues={initialValue}
        enableReinitialize={true}
        validationSchema={validationSchema}
        validateOnChange={false}
        validateOnBlur={true}
        onSubmit={() => {
          // do nothing
        }}
      >
        {(formikContext: FormikContextType<any>) => {
          return (
            <Modal
              onOk={() => {
                onOk(formikContext);
              }}
              onCancel={() => {
                onCancel(formikContext);
              }}
              {...rest}
            >
              <Form layout={layout} requiredMark={true}>
                {formSchema && (
                  <div className="dynamic__form">
                    <FormFieldRenderer schema={formSchema} name={""} />
                  </div>
                )}
              </Form>
            </Modal>
          );
        }}
      </Formik>
    </div>
  );
}
