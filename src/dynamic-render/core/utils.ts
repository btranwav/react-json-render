import { DisplayFields } from "../registries/component.registry";

export function getFieldByIndex(ifField: string, currentField: string) {
  if (!ifField) return "";

  if (ifField.indexOf("$index")) {
    const denpendentFieldParts = ifField.split(".");
    const currentFieldParts = currentField.split(".");

    const names = denpendentFieldParts.map((part, index) => {
      if (part === "$index") {
        return currentFieldParts[index];
      }

      return part;
    });

    return names.join(".");
  }

  return ifField;
}

export function isOnlyDisplayField(component: string) {
  return DisplayFields.includes(component);
}
