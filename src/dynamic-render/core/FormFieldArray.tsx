import React from 'react';
import { FieldArray, useField, useFormikContext } from "formik";
import { get } from "lodash";
import { FormFieldRenderer } from "./FormFieldRenderer";
import { Row } from 'antd';
import { FormFieldArrayProps } from '../types/core-types';

export function FormFieldArray({ schema, name, style, className, align, justify, flex }: FormFieldArrayProps) {
  const { values } = useFormikContext();
  const [, meta] = useField(name);

  const arrValues = get(values, name, []);

  style = { ...style, flex };

  return (
    <Row data-testid={name.replaceAll('.', '-')} 
      align={align} justify={justify}
      className={'dr__row dr__array ' + className}>
      <FieldArray
        name={name}
        render={() =>
          arrValues.map((_: unknown, index: number) => {
            const groupName = `${name}.${index}`;
            return (
              <FormFieldRenderer
                key={name + index}
                schema={schema}
                name={groupName}
              />
            );
          })
        }
      />
      {meta?.touched && meta?.error && typeof(meta?.error) === 'string' && (
        <><div style={{color: "#fa1326"}}>{meta?.error}</div></>)
      }
    </Row>
  );
}
