import React from "react";
import { FastField} from "formik";
import { getCustomComponent } from "../registries/component.registry";

export default function FormModalField({
  component,
  ...forwardProps
}: any) {

  const renderedComponent = getCustomComponent(component);

  return (<FastField as={renderedComponent} {...forwardProps} />);
}

