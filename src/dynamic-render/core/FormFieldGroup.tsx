import React, { useEffect, useState } from "react";
import { useField, useFormikContext } from "formik";
import { FormFieldRenderer } from "./FormFieldRenderer";
import { Row } from "antd";
import useCheckCondition from "../hooks/useCheckCondition";
import { isObject } from "lodash";
import { FormFieldGroupProps } from "../types/core-types";

export function FormFieldGroup({
  schema,
  name,
  className,
  hiddenIf,
  style = {},
  align,
  justify,
  flex,
}: FormFieldGroupProps) {
  const [, meta] = useField(name);
  const formikContext = useFormikContext();

  const [hidden, setHidden] = useState(false);
  const checkHiddenIf = useCheckCondition(name, hiddenIf, formikContext)();

  style = { ...style, flex };

  useEffect(() => {
    setHidden(checkHiddenIf);
  }, [checkHiddenIf]);

  return (
    <Row
      data-testid={name.replaceAll(".", "-")}
      align={align}
      justify={justify}
      className={"dr__row dr__group " + className}
      style={style}
    >
      {!hidden && <FormFieldRenderer name={name} schema={schema} />}
      {!hidden && meta.touched && meta.error && !isObject(meta.error) && (
        <div style={{ color: "#fa1326" }}>{meta.error}</div>
      )}
    </Row>
  );
}
