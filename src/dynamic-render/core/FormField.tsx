import React from "react";
import FormInputField from "./FormInputField";
import FormDisplayField from "./FormDisplayField";
import FormModalField from "./FormModalField";
import { isOnlyDisplayField as isNoneLabelField } from "./utils";
import { Row } from "antd";
import { FormFieldProps } from "../types/core-types";

export function FormField({ component, onlyDisplay, isModal, name, className, style, flex, align, justify, ...rest }: FormFieldProps) {

  const forwardProps = {
    ...rest,
    name,
    component,
  };

  style = { ...style, flex };

  if(isModal) {
    return (
      <FormModalField {...forwardProps} />
    );
  }
  else if (onlyDisplay || isNoneLabelField(component)) {
    const cssClasses = (className || '') + ' dr__row dr__display-field';

    return (
      <Row data-testid={name.replaceAll('.', '-')} align={align} justify={justify} className={cssClasses} style={style}>
        <FormDisplayField {...forwardProps} />
      </Row>
    );
  } else {
    const cssClasses = (className || '') + ' dr__row dr__input-field';
    
    return (
      <Row 
        data-testid={name.replaceAll('.', '-')} 
        align={align} justify={justify}
        className={cssClasses} style={style}>
        <FormInputField {...forwardProps} />
      </Row>
    );
  }
}
