import { Row } from "antd";
import React, { Children, cloneElement, isValidElement } from "react";

export default function DynamicRow({ children, className, justify, align, flex, style, ...rest }: any) {
  style = {...style, flex};

  const childrenWithProps = Children.map(children, (child) => {
    // Checking isValidElement is the safe way and avoids a TS error too.
    if (isValidElement(child)) {
      return cloneElement<any>(child);
    }

    return <React.Fragment key={Date.now()}>child</React.Fragment>;
  });

  return (
    <Row align={align} justify={justify} style={style}
      className={"dr__row " + className} 
      {...rest}>
      {childrenWithProps}
    </Row>
  );
}
