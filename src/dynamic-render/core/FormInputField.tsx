import React from "react";
import { ErrorMessage, FastField, useField, useFormikContext } from "formik";
import { useEffect, useState } from "react";
import { getCustomComponent } from "../registries/component.registry";
import useCheckCondition from "../hooks/useCheckCondition";
import { Form } from "antd";

export default function FormInputField({
  disabled,
  label,
  component,
  name,
  disabledIf,
  hiddenIf,
  ...rest
}: any) {
  const [, meta, helpers] = useField(name);
  const { error, touched } = meta;
  const { setValue } = helpers;

  const formikContext = useFormikContext();
  
  const [checkDisabledIf, setDisabledIf] = useState(false);
  const memoCheckDisabledIf = useCheckCondition(name, disabledIf, formikContext)();

  const [checkHiddenIf, setHiddenIf] = useState(false);
  const memoCheckHiddenIf = useCheckCondition(name, hiddenIf, formikContext)();

  useEffect(() => {
    setHiddenIf(memoCheckHiddenIf);
    // eslint-disable-next-line
  }, [memoCheckHiddenIf]);


  useEffect(() => {
    setDisabledIf(memoCheckDisabledIf);
    if (memoCheckDisabledIf) {
      setValue("", true);
    }
    // eslint-disable-next-line
  }, [memoCheckDisabledIf]);

  const forwardProps = {
    ...rest,
  };

  if(checkDisabledIf || !!disabled) {
    forwardProps['disabled'] = checkDisabledIf || !!disabled;
  } else {
    delete forwardProps['disabled'];
  }

  const renderedComponent = getCustomComponent(component);

  return (
    <>
      {checkHiddenIf && <></>}
      {!checkHiddenIf && (
        <Form.Item className="dr__form_item" label={label} validateStatus={touched && error ? 'error' : ''}>
          <FastField
            name={name}
            as={renderedComponent}
            {...forwardProps}
          />
          <ErrorMessage name={name} component="div" className="error-message" />
        </Form.Item>
        // </Box>
      )}
    </>
  );
}
