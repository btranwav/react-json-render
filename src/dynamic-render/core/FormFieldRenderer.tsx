import React from "react";
import { FormField } from "./FormField";
import { FormFieldArray } from "./FormFieldArray";
import { FormFieldGroup } from "./FormFieldGroup";
import DynamicRow from "./DynamicRow";
import { compact, has, isArray } from "lodash";
import { FormFieldTabs } from "./FormFieldTabs";
import { CommonProps, FormFieldArrayProps, FormFieldRendererProps } from "../types/core-types";
import { ComponentFieldProps, FieldSchemaProps, GroupProps } from "../types/schema-types";

export function FormFieldRenderer(props: FormFieldRendererProps) {
  const { schema, name } = props;
  return (
    <>
      {Object.keys(schema)
        .filter((key) => {
          const fieldConfig = schema[key];
          return (
            typeof fieldConfig !== "string" && typeof fieldConfig !== "number" && key !== 'style'
          );
        })
        .map((key: string, index: number) => {

          const fieldConfig: FieldSchemaProps | FieldSchemaProps[] | ComponentFieldProps | FormFieldArrayProps = schema[key];
          const hierrarchyName = buildHierrachyName(name, key);
          const elementKey = hierrarchyName + index;

          if (isArray(fieldConfig)) {
            const template = fieldConfig[0];
            const {className} = fieldConfig[0] as FormFieldArrayProps;
            return (
              <FormFieldArray
                key={elementKey}
                name={hierrarchyName}
                schema={template}
                className={className}
              />
            );
          } else {
            if (has(fieldConfig, 'group')) {
              const { group } = fieldConfig as FieldSchemaProps;
              const { hiddenIf, className, style, ...components } = group as GroupProps;
              return (
                <FormFieldGroup
                  key={elementKey}
                  name={hierrarchyName}
                  schema={components}
                  className={className}
                  hiddenIf={hiddenIf}
                  style={style}
                />
              );
            } else if (has(fieldConfig, 'tabs')) {
              const { tabs } = fieldConfig as FieldSchemaProps;
              return (
                <FormFieldTabs
                  key={elementKey}
                  name={hierrarchyName}
                  tabs={tabs || []}
                />
              );
            } else if (has(fieldConfig, 'component')) {
              const {component, className, style, onlyDisplay, ...rest} = fieldConfig as ComponentFieldProps;
              return (
                <FormField
                  key={elementKey}
                  name={hierrarchyName}
                  component={component}
                  className={className}
                  style={style}
                  {...rest}
                />
              );
            } else {
              const { className, style, align, justify, flex } = fieldConfig as CommonProps;
              return (
                <DynamicRow
                  data-testid={buildComponentId(name, key)}
                  key={elementKey}
                  className={className}
                  style={style}
                  align={align}
                  justify={justify}
                  flex={flex}
                >
                  <FormFieldRenderer
                    name={hierrarchyName}
                    schema={fieldConfig}
                  />
                </DynamicRow>
              );
            }
          }
        })}
    </>
  );
}

export function buildHierrachyName(parentName: string, key: string) {
  const name = key.startsWith("wrapper") ? "" : key;

  if (parentName) {
    return parentName + (name ? "." + name : name);
  }

  return name;
}

export function buildComponentId(parentName: string, key: string) {
  return compact([parentName, key]).join('-').replaceAll('.', '-');
}
