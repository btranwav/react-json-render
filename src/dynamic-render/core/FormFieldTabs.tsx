import React from "react";
import { useFormikContext } from "formik";
import { Row, Tabs } from "antd";
import { FormFieldGroup } from "./FormFieldGroup";
import { getEventHandler } from "../registries/event.registry";
import { FormFieldTabsProps } from "../types/core-types";
import { GroupProps } from "../types/schema-types";

export function FormFieldTabs({ tabs = [], onTabChangeFn = '', name }: FormFieldTabsProps) {
  const formikContext = useFormikContext();
  const onTabChange = getEventHandler(onTabChangeFn, formikContext);

  return (
    <Row data-testid={name.replaceAll('.', '-')} 
      className={'dr__row dr__tabs'}>
      <Tabs defaultActiveKey="1" onChange={onTabChange} centered type="card" className="dr__tab_pane">
        {tabs.map((tab: any, index: number) => {
          const { hiddenIf, className, style, ...components } = tab.group as GroupProps;
          return (
            <Tabs.TabPane tab={tab.tabName} key={index}>
              <FormFieldGroup
                key={`${name}.${index}`}
                schema={components}
                className={className}
                style={style}
                hiddenIf={hiddenIf}
                name={`${name}.${index}`}/>
            </Tabs.TabPane>
          );
        })}
      </Tabs>
    </Row>
  );
}
