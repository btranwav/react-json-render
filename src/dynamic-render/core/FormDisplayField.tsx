import React from "react";
import { FastField, useField, useFormikContext } from "formik";
import { useEffect, useState } from "react";
import { getCustomComponent } from "../registries/component.registry";
import useCheckCondition from "../hooks/useCheckCondition";

export default function FormDisplayField({
  name,
  component,
  hiddenIf,
  ...forwardProps
}: any) {
  const [, meta] = useField(name);
  const { value } = meta;

  const renderedComponent = getCustomComponent(component);
  const props = { ...forwardProps, value, name };

  const formikContext = useFormikContext();

  const [hidden, setHidden] = useState(false);
  const checkHiddenIf = useCheckCondition(name, hiddenIf, formikContext)();

  useEffect(() => {
    setHidden(checkHiddenIf);
  }, [checkHiddenIf]);

  return (<>{!hidden && <FastField as={renderedComponent} {...props} />}</>);
}

