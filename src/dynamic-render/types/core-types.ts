import { CSSProperties } from "react";
import { TabsProps } from "./schema-types";

export type FormFieldProps = {
    name: string;
    component: string;
    onlyDisplay?: boolean;
    style: CSSProperties;
    className: string;
    value?: any,
    [key: string]: any;
}
//  & UnknowProps;

export type FormFieldArrayProps = {
    schema: any;
    name: string;
    className?: string;
    align?: "top" | "middle" | "bottom" | "stretch";
    justify?: "start" | "end" | "center" | "space-around" | "space-between";
    flex?: number;
    style?: CSSProperties;
}

export type FormFieldGroupProps = {
    name: string,
    schema: UnknowProps
    className?: string,
    style?: CSSProperties,
    hiddenIf?: any,
    align?: "top" | "middle" | "bottom" | "stretch",
    justify?: "start" | "end" | "center" | "space-around" | "space-between";
    flex?: number;
}

export type FormFieldRendererProps = {
    schema: any;
    name: string;
    className?: any;
}

export type FormFieldTabsProps = {
    name: string,
    tabs: TabsProps,
    onTabChangeFn?: string,
}

export type FormInputFieldProps = {
    
}

export type FormDisplayFieldProps = {

}

export type DynamicRowProps = {
    
}

export type DynamicFormProps = {

}


export type UnknowProps = {
    [k: string]: FormFieldProps
}

export type CommonProps = {
    className: string;
    style: CSSProperties;
} & UnknowProps;










