export type FormConfig = {
    uniqueName: string,
    validationSchema: any,
    initialValue: any,
    formSchema: any
  }