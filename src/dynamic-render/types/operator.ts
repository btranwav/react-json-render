import { FormikContextType } from "formik";
import { getFieldByIndex } from "../core/utils";
import { isEmpty } from "lodash";

export enum Op {
  includes = "includes",
  greater = "greater",
  greaterEquals = "greaterEquals",
  less = "less",
  lessEquals = "lessEquals",
  equals = "equals",
  notEquals = "notEquals",
  isEmpty = "isEmpty",
  isNotEmpty = "isNotEmpty",
}

export type Condition = {
  field: string;
  value?: any;
  op: Op;
};

export type ConditionType = Condition | [Condition];

export function compareWithOp(
  lefOperand: any,
  rightOperand: any,
  op: Op
): boolean {
  switch (op) {
    case Op.includes:
      return (lefOperand || []).includes(rightOperand);
    case Op.equals:
      return lefOperand === rightOperand;
    case Op.notEquals:
      return lefOperand !== rightOperand;
    case Op.greater:
      return lefOperand > rightOperand;
    case Op.greaterEquals:
      return lefOperand >= rightOperand;
    case Op.less:
      return lefOperand < rightOperand;
    case Op.lessEquals:
      return lefOperand <= rightOperand;
    case Op.isEmpty:
      return isEmpty(lefOperand);
    case Op.isNotEmpty:
      return !isEmpty(lefOperand);
    default:
      return true;
  }
}

export const evaluateCondition = (
  condition: Condition,
  soureFieldName: string,
  { getFieldMeta }: FormikContextType<any>
) => {
  const hiddenIfFieldName = getFieldByIndex(condition.field, soureFieldName);
  return compareWithOp(
    getFieldMeta(hiddenIfFieldName).value,
    condition.value,
    condition.op
  );
};
