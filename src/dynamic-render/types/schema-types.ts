import { CSSProperties } from "react";

export type InputProps = {
  onClickFn: string,
}

export type ComponentStateProps = {
  disabledIf: {
    field: string,
    value: string | boolean,
    resetValue?: boolean
  },
  requiredIf: {
    field: string,
    value: string | boolean,
  },
}

export type ForwardProps = {
  [key: string]: any
}

export type TabProps = {
  tabName: string;
  group: GroupProps;
}

export type TabsProps = TabProps[];

export type Schema = {
  component: string,
  label: string,
  onlyDisplay: boolean,
  style: any,
} & ComponentStateProps & ForwardProps;

export type FieldSchemaProps = {
  group?: GroupProps;
  tabs?: TabsProps;
};

export type ComponentFieldProps = {
  component: string;
  // belong to component's container
  onlyDisplay?: boolean;
  label: string;
  className: string;
  style: CSSProperties;
  // belong to the main component
  settings: any
}


export type GroupProps = {
  name: string,
  className?: string,
  style?: CSSProperties,
  hiddenIf?: any;
  [key: string]: any;
}



