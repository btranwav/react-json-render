export interface PaginationRequest<T> {
  page?: number;
  limit?: number;
  filter?: T;
  order?: any;
}

export interface PaginationResponse<T> {
  count: number;
  limit: number;
  page: number;
  rows: T[];
}