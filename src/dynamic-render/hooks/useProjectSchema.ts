import { useEffect, useState } from "react";
import { loadJsonFile } from "../load-script";

export default function useProjectSchema(project: string, projectFilePath: string, schemas: any) {
  const [validationSchema, setValidationSchema] = useState(null);
  const [formSchema, setFormSchema] = useState(null);
  const [initialValue, setInitialValue] = useState({});
  const [error, setError] = useState(false);

  useEffect(() => {

    loadJsonFile(projectFilePath).then(data => {
      const projectName = data[project];
      const result = schemas[projectName];
  
      if (!result) {
        alert("Project not found");
        setError(true);
      } else {
        setValidationSchema(result.validationSchema);
        setInitialValue(result.initialValue);
        setFormSchema(result.formSchema);
      }
    })
    .catch(() => {
      console.log('Failed to load project mappings');
      setError(true);
    });
    // eslint-disable-next-line
  }, []);

  return {validationSchema, formSchema, initialValue, error};
}