import { FieldMetaProps } from "formik";
import { useMemo } from "react";
import { getFieldByIndex } from "../core/utils";

/*
  targetFields: array of field names which can contain $index
  currentFieldName: must be descendants of target fields
*/
export default function useGetFieldValues(
  targetFields: string[],
  getFieldMeta: (name: string) => FieldMetaProps<unknown>,
  currentFieldName: string) {
  return useMemo(() => (): any[] => {
    return (targetFields || []).map(targetField => {
      const targetFieldName = getFieldByIndex(targetField, currentFieldName);
      return getFieldMeta(targetFieldName).value;
    });
  }, [targetFields, getFieldMeta, currentFieldName]);
}