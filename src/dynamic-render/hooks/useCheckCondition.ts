import { FormikContextType } from "formik";
import { useMemo } from "react";
import { Condition, ConditionType, evaluateCondition } from "../types/operator";

export default function useCheckCondition(
  currentFieldName: string,
  condition: ConditionType,
  formikContext: FormikContextType<any>
): () => boolean {
  return useMemo(
    () => (): any => {
      if (condition) {
        if (condition instanceof Array) {
          const conds = condition.map((h: Condition) => {
            if (h instanceof Array) {
              const childConds = h.map((hh: Condition) => {
                return evaluateCondition(hh, currentFieldName, formikContext);
              });
              return !childConds.includes(false);
            } else {
              return evaluateCondition(h, currentFieldName, formikContext);
            }
          });

          return conds.includes(true);
        } else if (condition.field) {
          return evaluateCondition(condition, currentFieldName, formikContext);
        }
      }

      return false;
    },
    // eslint-disable-next-line
    [condition, currentFieldName, formikContext]
  );
}
