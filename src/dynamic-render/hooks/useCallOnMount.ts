import { useEffect, useRef } from "react";

export function useCallOnMount(fn: () => void) {
    const isCalled = useRef(false);

    useEffect(() => {
        if (!isCalled.current) {
            isCalled.current = true;
            fn();
        }
    }, [fn]);
}
