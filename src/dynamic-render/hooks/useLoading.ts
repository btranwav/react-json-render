import { set } from "lodash";
import { FormConfig } from "../types/form-config";

/*
  targetFields: array of field names which can contain $index
  currentFieldName: must be descendants of target fields
*/
export default function useLoading(formConfig: FormConfig, defaultValue = true) {
  set(formConfig, 'initialValue.isLoading', defaultValue);
  set(formConfig, 'formSchema.loadingComp', {
    component: 'loading',
    hiddenIf: {
      field: 'isLoading',
      value: false
    }
  });
  return formConfig;
}