export default function loadScript(url: string, callback: any) {
  console.info("Loading ", url);
  const script: any = document.createElement("script");
  script.type = "text/javascript";

  if (script.readyState) {
    //IE
    script.onreadystatechange = function () {
      if (script.readyState === "loaded" || script.readyState === "complete") {
        script.onreadystatechange = null;
        callback();
      }
    };
  } else {
    //Others
    script.onload = function () {
      callback();
    };
  }

  script.src = url;
  document.getElementsByTagName("head")[0].appendChild(script);
}


export function loadScriptAsync(url: string) {
  return new Promise((resolve) => {
    console.info("Loading ", url);
    const script: any = document.createElement("script");
    script.type = "text/javascript";
  
    if (script.readyState) {
      //IE
      script.onreadystatechange = function () {
        if (script.readyState === "loaded" || script.readyState === "complete") {
          script.onreadystatechange = null;
          resolve('');
        }
      };
    } else {
      //Others
      script.onload = function () {
        resolve('');
      };
    }
  
    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
  });
} 

export async function loadJsonFile(url: string) {
  return fetch(url).then(res => res.json());
}