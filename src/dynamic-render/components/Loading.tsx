import React, { useEffect, useMemo, useState } from "react";
import { useFormikContext } from "formik";
import { Spin } from "antd";

export default function Loading() {
  const formikContext = useFormikContext();
  const { getFieldMeta } = formikContext;
  const [isLoading, setLoading] = useState(true);

  const loadingStatus = useMemo(() => {
    return getFieldMeta<boolean>("isLoading").value;
  }, [getFieldMeta]);

  useEffect(() => {
    setLoading(loadingStatus);
  }, [loadingStatus]);

  return (
    <>
      {isLoading && (
        <div className="loading">
          <Spin size="large" />
        </div>
      )}
    </>
  );
}
