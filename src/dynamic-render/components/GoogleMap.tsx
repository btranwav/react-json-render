/**
 * Copyright 2021 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { PlusCircleOutlined, SearchOutlined } from "@ant-design/icons";
import { Button, Form, Input, Row } from "antd";
import React, { useRef, useEffect, useState } from "react";
import { ErrorMessage } from "formik";

function convertToDms(dd: number, isLng: boolean) {
  var dir = dd < 0 ? (isLng ? "W" : "S") : isLng ? "E" : "N";

  var absDd = Math.abs(dd);
  var deg = absDd | 0;
  var frac = absDd - deg;
  var min = (frac * 60) | 0;
  var sec = frac * 3600 - min * 60;
  // Round it to 2 decimal points.
  sec = Math.round(sec * 100) / 100;
  return deg + "°" + min + "'" + sec + '"' + dir;
}

function isIconMouseEvent(
  e: google.maps.MapMouseEvent | google.maps.IconMouseEvent
): e is google.maps.IconMouseEvent {
  return "placeId" in e;
}

function getAddress(address_components: any[]) {
  const address: any = {};
  for (var i = 0; i < address_components.length; i++) {
    if (
      address_components[i].types[0] === "administrative_area_level_1" &&
      address_components[i].types[1] === "political"
    ) {
      address.state = address_components[i].long_name;
    }
    if (
      address_components[i].types[0] === "locality" &&
      address_components[i].types[1] === "political"
    ) {
        address.city = address_components[i].long_name;
    }

    if (address_components[i].types[0] === "postal_code") {
        address.zipcode = address_components[i].long_name;
    }

    if (address_components[i].types[0] === "country") {
        address.country = address_components[i].long_name;
    }
  }

  console.log(address);
  return address;
}

function MapComponent({
  center,
  zoom,
}: {
  center: google.maps.LatLngLiteral;
  zoom: number;
}) {
  const ref = useRef<any>(null);
  const searchRef = useRef<any>(null);
  const [map, setMap] = React.useState<any>();
  const [searchBox, setSearchBox] = React.useState<any>();
  const [placesService, setPlaceService] = React.useState<any>();
  const [infoWindow, setInfoWindow] = useState<any>();
  const [infowindowContent, setInfoWindowContent] = useState<any>();
  const [companyName, setCompanyName] = useState<any>();
  const [assetName] = useState<any>();
  const [assetType] = useState<any>();
  const [postCode, setpostCode] = useState<any>();
  const [city, setcity] = useState<any>();
  const [address, setaddress] = useState<any>();
  const [state, setstate] = useState<any>();
  const [country, setcountry] = useState<any>();
  const [lattitude, setlattitude] = useState<any>();
  const [longtitude, setlongtitude] = useState<any>();

  useEffect(() => {
    if (ref.current && !map) {
      const mapInstance = new window.google.maps.Map(ref.current, {
        center,
        zoom,
      });
      setMap(mapInstance);
      setPlaceService(new google.maps.places.PlacesService(mapInstance));
      setInfoWindow(new google.maps.InfoWindow());
      setInfoWindowContent(document.getElementById("infowindow-content"));
    }

    if (searchRef.current && !map) {
      setSearchBox(
        new window.google.maps.places.SearchBox(searchRef.current.input)
      );
    }
  }, [ref, searchRef, map, center, zoom]);

  useEffect(() => {
    if (map && searchBox && infoWindow) {
      map.controls[google.maps.ControlPosition.TOP_LEFT].push(searchBox);
      infoWindow.setContent(infowindowContent);

      map.addListener(
        "click",
        (event: google.maps.MapMouseEvent | google.maps.IconMouseEvent) => {
          // If the event has a placeId, use it.
          if (isIconMouseEvent(event)) {
            // Calling e.stop() on the event prevents the default info window from
            // showing.
            // If you call stop here when there is no placeId you will prevent some
            // other map click event handlers from receiving the event.
            event.stop();

            if (event.placeId) {
              placesService.getDetails(
                { placeId: event.placeId },
                (
                  place: google.maps.places.PlaceResult | null,
                  status: google.maps.places.PlacesServiceStatus
                ) => {
                  console.log("You clicked on place:", place);
                  if (
                    status === "OK" &&
                    place &&
                    place.geometry &&
                    place.geometry.location
                  ) {
                    infoWindow.close();
                    setCompanyName(place.name);
                    const address = getAddress(place.address_components || []);
                    setaddress(place.vicinity);
                    setlattitude(place.geometry.location.lat());
                    setlongtitude(place.geometry.location.lng());
                    setcity(address.city);
                    setcountry(address.country);
                    setpostCode(address.zipcode);
                    setstate(address.state);
                    infoWindow.setPosition(place.geometry.location);
                    infoWindow.open(map);
                  }
                }
              );
            }
          }
        }
      );

      // Bias the SearchBox results towards current map's viewport.
      map.addListener("bounds_changed", () => {
        searchBox.setBounds(map.getBounds() as google.maps.LatLngBounds);
      });
      let markers: google.maps.Marker[] = [];

      // Listen for the event fired when the user selects a prediction and retrieve
      // more details for that place.
      searchBox.addListener("places_changed", () => {
        const places = searchBox.getPlaces();

        if (places.length === 0) {
          return;
        }

        // Clear out the old markers.
        markers.forEach((marker) => {
          marker.setMap(null);
        });
        markers = [];

        // For each place, get the icon, name and location.
        const bounds = new google.maps.LatLngBounds();

        places.forEach((place: any) => {
          if (!place.geometry || !place.geometry.location) {
            console.log("Returned place contains no geometry");
            return;
          }

          const icon = {
            url: place.icon as string,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(25, 25),
          };

          // Create a marker for each place.
          markers.push(
            new google.maps.Marker({
              map,
              icon,
              title: place.name,
              position: place.geometry.location,
            })
          );

          if (place.geometry.viewport) {
            // Only geocodes have viewport.
            bounds.union(place.geometry.viewport);
          } else {
            bounds.extend(place.geometry.location);
          }
        });
        map.fitBounds(bounds);
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [map, searchBox, placesService]);

  return (
    <div style={{ width: "100%" }}>
      <Input
        style={{
          position: "absolute",
          zIndex: 10,
          width: "300px",
          margin: "20px",
        }}
        ref={searchRef}
        className="dr_input"
        type="text"
        placeholder="Search Box"
        prefix={<SearchOutlined rev={undefined} />}
      />
      <div
        style={{ display: "flex", height: "100%", width: "100%" }}
        ref={ref}
        id="map"
      />
      <div id="infowindow-content" style={{ zIndex: 11 }}>
        <Row justify={"space-between"} className="dr__row">
          <Form.Item
            label={<h4>Company Name</h4>}
            required={true}
            style={{ width: "100%" }}
          >
            <Input type="text" value={companyName} />
            <ErrorMessage
              name="companyName"
              component="div"
              className="error-message"
            />
          </Form.Item>
        </Row>
        <Row justify={"space-between"}>
          <h4>Asset</h4>
          <Button type="link" icon={<PlusCircleOutlined rev={undefined} />}>
            Add new asset
          </Button>
        </Row>
        <Row justify={"space-between"} className="dr__row">
          <Form.Item label={<h4>Asset Name</h4>} required={true}>
            <Input type="text" value={assetName} />
            <ErrorMessage
              name="assetName"
              component="div"
              className="error-message"
            />
          </Form.Item>
          <Form.Item label={<h4>Asset Type</h4>} required={true}>
            <Input type="text" value={assetType} />
            <ErrorMessage
              name="assetType"
              component="div"
              className="error-message"
            />
          </Form.Item>
        </Row>
        <Row justify={"space-between"} className="dr__row">
          <Form.Item
            label={<h4>Address</h4>}
            required={true}
            style={{ width: "100%" }}
          >
            <Input type="text" value={address} />
            <ErrorMessage
              name="address"
              component="div"
              className="error-message"
            />
          </Form.Item>
        </Row>
        <Row justify={"space-between"} className="dr__row">
          <Form.Item label={<h4>Post Code</h4>} required={true}>
            <Input type="text" value={postCode} />
            <ErrorMessage
              name="postCode"
              component="div"
              className="error-message"
            />
          </Form.Item>
          <Form.Item label={<h4>City</h4>} required={true}>
            <Input type="text" value={city} />
            <ErrorMessage
              name="city"
              component="div"
              className="error-message"
            />
          </Form.Item>
        </Row>
        <Row justify={"space-between"} className="dr__row">
          <Form.Item label={<h4>State</h4>} required={true}>
            <Input type="text" value={state} />
            <ErrorMessage
              name="state"
              component="div"
              className="error-message"
            />
          </Form.Item>
          <Form.Item label={<h4>Country</h4>} required={true}>
            <Input type="text" value={country} />
            <ErrorMessage
              name="country"
              component="div"
              className="error-message"
            />
          </Form.Item>
        </Row>
        <Row justify={"space-between"} className="dr__row">
          <Form.Item label={<h4>Lattitude</h4>} required={true}>
            <Input type="text" value={convertToDms(lattitude, false)} />
            <ErrorMessage
              name="lattitude"
              component="div"
              className="error-message"
            />
          </Form.Item>
          <Form.Item label={<h4>Longtitude</h4>} required={true}>
            <Input type="text" value={convertToDms(longtitude, true)} />
            <ErrorMessage
              name="longtitude"
              component="div"
              className="error-message"
            />
          </Form.Item>
        </Row>
        <Row justify={"space-between"} className="dr__row">
          <Button type="primary" style={{ width: "100%" }}>
            Submit
          </Button>
        </Row>
      </div>
    </div>
  );
}

export default MapComponent;
