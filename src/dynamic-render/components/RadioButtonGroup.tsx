import React, { useEffect, useState } from "react";
import { isEqual } from "lodash";
import { useFormikContext } from "formik";
import { getEventHandler } from "../registries/event.registry";
import useGetFieldValues from "../hooks/useGetFieldValue";
import { Radio } from "antd";

type OptionFn = {
  funcName: string;
  dependentFields: string[];
};

type Option = {
  label: string;
  value: string;
};

// interface Props {
//   value: string;
//   options: Option[];
//   disabled: boolean;
//   onChange: Fun1<any>;
//   style?: any;
//   optionfn: OptionFn;
//   field: any;
// }

const RadioButtonGroup = ({ disabled, settings, name, ...rest }: any) => {
  const { options, optionfn = {} } = settings;

  const optionFn: OptionFn = optionfn;
  const formikContext = useFormikContext();
  const [internalOptions, setOptions] = useState<Option[]>(options || []);

  let filterFn: any = null;
  let getDependentFieldValues: any = null;

  if (optionFn?.funcName) {
    filterFn = getEventHandler(optionFn.funcName);
  }

  getDependentFieldValues = useGetFieldValues(
    optionFn?.dependentFields,
    formikContext.getFieldMeta,
    name
  );

  useEffect(() => {
    let mounted = true;

    if (filterFn) {
      filterFn(getDependentFieldValues()).then((result: Option[]) => {
        if (isEqual(result, internalOptions)) { // prevent re-render
          return;
        }
        if (mounted) {
          setOptions(result);
          // if (result.length > 0) {
          //   if (isNull(value) || isUndefined(value)) {
          //     onChange(result[0].value);
          //   }
          // }
        }
      });
    }

    return () => {
      mounted = false;
    };
    // eslint-disable-next-line
  }, [getDependentFieldValues]);

  function buildOptions(_options: Option[] = []) {
    return _options
      .filter((o) => o.label)
      .map((o: any, index: number) => {
        return (
          <Radio key={'radio-button-' + index} value={o.value}>
            {o.label}
          </Radio>
        );
      });
  }

  return (
    <Radio.Group name={name} {...rest}>
      {buildOptions(internalOptions)}
    </Radio.Group>
  );
};

export default RadioButtonGroup;
