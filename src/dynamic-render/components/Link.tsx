import React from "react";
import { Button } from "antd";
import { useFormikContext } from "formik";
import { getEventHandler } from "../registries/event.registry";

export function DynamicLink({ settings }: any) {
  const {text, onClickFn, ...rest} = settings;
  const formikContext = useFormikContext();

  let onClick = null;
  if (onClickFn) {
    onClick = getEventHandler(onClickFn, formikContext);
  }

  return (
    <Button type="link" onClick={onClick} {...rest}>{text}</Button>
  );
}
