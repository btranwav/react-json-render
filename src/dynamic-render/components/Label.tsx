import React from "react";

export function LabelField({value, settings}: any) {
  const { text, className, style } = settings;
  return (
      <label className={className} style={style}>{text || value}</label>
  );
}
