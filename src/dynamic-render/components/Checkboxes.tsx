import { Checkbox } from "antd";
import { useFormikContext } from "formik";
import { isEqual } from "lodash";
import React, { useEffect, useState } from "react";
import useGetFieldValues from "../hooks/useGetFieldValue";
import { getEventHandler } from "../registries/event.registry";

type Option = {
  label: string;
  value: string | number;
};

type OptionFn = {
  funcName: string;
  dependentFields: string[];
};

const Checkboxes = ({ name, onChange, settings, ...rest }: any) => {
  const { options, optionfn } = settings;

  const optionFn: OptionFn = optionfn || {};
  const formikContext = useFormikContext();
  const [checkboxOptions, setOptions] = useState<Option[]>(options);

  let filterFn: any = null;
  if (optionFn?.funcName) {
    filterFn = getEventHandler(optionFn.funcName);
  }

  const getDependentFieldValues = useGetFieldValues(
    optionFn?.dependentFields,
    formikContext.getFieldMeta,
    name
  );

  useEffect(() => {
    let mounted = true;

    if (filterFn) {
      filterFn(getDependentFieldValues()).then((result: Option[]) => {
        if (isEqual(result, checkboxOptions)) {
          // prevent re-render
          return;
        }
        if (mounted) {
          setOptions(result);
        }
      });
    }

    return () => {
      mounted = false;
    };
    // eslint-disable-next-line
  }, [getDependentFieldValues]);

  return (
    <Checkbox.Group
      options={checkboxOptions}
      onChange={(evt) => {
        formikContext.setFieldTouched(name, true);
        formikContext.setFieldValue(name, evt, true);
      }}
      {...rest}
    />
  );
};

export default Checkboxes;
