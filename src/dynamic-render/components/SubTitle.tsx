import React from "react";
import Title from "antd/lib/typography/Title";
import { Col } from "antd";

export function SubTitle({ value, name, settings }: any) {
  return (
    <Col className="sub__title" {...settings}>
      <Title level={4}>{value}</Title>
    </Col>
  );
}
