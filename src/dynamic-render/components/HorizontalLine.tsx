import { Divider } from "antd";
import React from "react";

export function HorizontalLine() {
  return <Divider className={'horizontal-line'} />;
}
