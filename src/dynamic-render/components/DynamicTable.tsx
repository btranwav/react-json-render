import { Table, message } from "antd";
import { useFormikContext } from "formik";
import React from "react";
import { useEffect, useState } from "react";
import { getEventHandler } from "../registries/event.registry";
import { buildTableColumns } from "./table.utils";

export function DynamicTable({ settings }: any) {
    const [messageApi, contextHolder] = message.useMessage();
    const { columns, loadDataFn, paginationFn, onRowSelectedFn, ...rest } = settings;
    const formikContext = useFormikContext();
    const loadData = getEventHandler(loadDataFn, formikContext);
    const pagination = getEventHandler(paginationFn, formikContext)();
    const tableColumns = buildTableColumns(columns, formikContext);
    const onRowSelected = getEventHandler(onRowSelectedFn, formikContext, {messageApi});
    const [data, setData] = useState<any>();
    const [total, setTotal] = useState<number>();
    const [page, setPage] = useState(1);
    const [pageSize, setPageSize] = useState(pagination.pageSize);

    useEffect(() => {
        loadData(page - 1, pageSize).then(([total, result]: any) => {
            setData(result);
            setTotal(total);
        });

        return () => setData(null);
        // eslint-disable-next-line
    }, [page, pageSize]);

    return (
        <div className="dr_table">
            {contextHolder}
            <React.StrictMode>
                <Table
                    columns={tableColumns}
                    dataSource={data}
                    pagination={{
                        ...pagination, 
                        onChange: (page: number, pageSize: number) => {
                            setPage(page);
                            setPageSize(pageSize);
                        },
                        total
                    }}
                    rowSelection={!onRowSelectedFn ? undefined : {
                        selections: [
                            Table.SELECTION_ALL,
                            Table.SELECTION_INVERT,
                            Table.SELECTION_NONE
                        ],
                        type: 'checkbox',
                        onChange: onRowSelected,
                    }}
                    {...rest}
                />
            </React.StrictMode>
        </div>
    )
}