import { Input } from "antd";
import React from "react";
import getIcon from "../registries/icon.registry";
import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';
import { clone } from "lodash";

export default function InputPassword({ settings, disabled, ...rest }: any) {
  const cloneSettings = clone(settings || {});
  const prefixComp = getIcon(cloneSettings?.prefix);
  delete cloneSettings?.prefix;
  delete cloneSettings?.className;

  return (
    <Input.Password
      className="dr_input"
      prefix={prefixComp}
      iconRender={(visible) => (visible ? <EyeTwoTone rev={undefined} /> : <EyeInvisibleOutlined rev={undefined} />)}
      disabled={disabled}
      {...cloneSettings}
    />
  );
}
