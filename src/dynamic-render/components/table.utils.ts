import { FormikContextType } from "formik";
import { getEventHandler } from "../registries/event.registry";

export function buildTableColumns(
  columns: any[],
  formikContext: FormikContextType<any>
) {
  return columns.map((col) => {
    return {
      title: col.title,
      dataIndex: col.dataIndex,
      key: col.dataIndex,
      render: !col.renderFn ? undefined : getEventHandler(col.renderFn || "", formikContext),
    };
  });
}