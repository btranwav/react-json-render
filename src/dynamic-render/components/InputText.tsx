import { Input } from "antd";
import React from "react";
import getIcon from "../registries/icon.registry";
import { clone } from "lodash";

export default function InputText({ settings, disabled, ...rest }: any) {
  const cloneSettings = clone(settings || {});
  const prefixComp = getIcon(cloneSettings?.prefix);
  delete cloneSettings?.prefix;
  delete cloneSettings?.className;

  return (
    <Input
      className={"dr_input " + settings?.className}
      prefix={prefixComp}
      disabled={disabled}
      {...rest}
      {...cloneSettings}
    />
  );
}
