import React from "react";
import { Button, ConfigProvider } from "antd";
import { useFormikContext } from "formik";
import { getEventHandler } from "../registries/event.registry";
import getIcon from "../registries/icon.registry";

export function Icon({ settings }: any) {
  const { onClickFn, icon, ...rest } = settings;
  const formikContext = useFormikContext();

  let onClick = null;
  if (onClickFn) {
    onClick = getEventHandler(onClickFn, formikContext);
  }

  const prefixComp = getIcon(icon);

  return (
    <ConfigProvider
      theme={{
        token: {
          colorPrimary: "rgba(0, 0, 0, 0.88)",
        },
      }}
    >
      <Button icon={prefixComp} onClick={onClick} {...rest} />
    </ConfigProvider>
  );
}
