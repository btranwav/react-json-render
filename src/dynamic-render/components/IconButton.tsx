import React from "react";
import { Button } from "antd";
import { useFormikContext } from "formik";
import { getEventHandler } from "../registries/event.registry";
import getIcon from "../registries/icon.registry";
import { useNavigate } from "react-router-dom";

export function IconButton({ settings }: any) {
  const { onClickFn, type = "primary", icon, text, ...rest } = settings;
  const formikContext = useFormikContext();
  const navigate = useNavigate();

  let onClick = null;
  if (onClickFn) {
    onClick = getEventHandler(onClickFn, formikContext, navigate);
  }

  const prefixComp = getIcon(icon);

  return (
      <Button
        className="dr_icon_button"
        icon={prefixComp}
        type={type}
        onClick={onClick}
        {...rest}
      >
        {text}
      </Button>
  );
}
