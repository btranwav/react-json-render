import { Select } from "antd";
import { useFormikContext } from "formik";
import { isEqual } from "lodash";
import React, { useEffect, useState } from "react";
import useGetFieldValues from "../hooks/useGetFieldValue";
import { getEventHandler } from "../registries/event.registry";

type Option = {
  label: string;
  value: string;
};

const InputSelect = (props: any) => {
  const { value, name, disabled, settings } = props;
  const { options = [], optionfn = {}, onChangeFn, ...rest } = settings;
  const formikContext = useFormikContext();
  const [selectOptions, setOptions] = useState<Option[]>(options);
  const [dependentFieldValues, setDependentFieldValues] = useState<any>([]);

  let filterFn: any = null;
  if (optionfn.funcName) {
    filterFn = getEventHandler(optionfn.funcName, formikContext);
  }

  let onValueChange: any = null;
  if (onChangeFn) {
    onValueChange = getEventHandler(onChangeFn, formikContext, {currentFieldName: name});
  }

  const getDependentFieldValues = useGetFieldValues(
    optionfn?.dependentFields,
    formikContext.getFieldMeta,
    name
  );

  useEffect(() => {
    const newValues = getDependentFieldValues();
    const diff = !isEqual(dependentFieldValues, newValues);
    if (diff) {
      setDependentFieldValues(newValues);
    }
    // eslint-disable-next-line
  }, [getDependentFieldValues]);

  useEffect(() => {
    if (filterFn) {
      filterFn(dependentFieldValues, name).then((result: Option[]) => {
        if (isEqual(result, selectOptions)) {
          // prevent re-render
          return;
        }

        setOptions(result);
      });
    }

    // eslint-disable-next-line
  }, [dependentFieldValues]);

  return (
    <Select
      value={value}
      onChange={(evt) => {
        formikContext.setFieldTouched(name, true);
        formikContext.setFieldValue(name, evt, true);
        onValueChange && onValueChange(evt);
      }}
      disabled={disabled}
      options={selectOptions}
      {...rest}
    />
  );
};

export default InputSelect;
