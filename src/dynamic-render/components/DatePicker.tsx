import React from "react";
import { DatePicker } from "antd";
import moment from "moment";

const DateTimeField = ({value, onChange, settings}: any) => {
  const { ...rest } = settings;

  return (
    <DatePicker 
      value={moment(value)}
      onChange={(evt) => onChange(evt?.toJSON())} {...rest}></DatePicker>
    
  );
};

export default DateTimeField;
