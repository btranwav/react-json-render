import React from "react";
import Title from "antd/lib/typography/Title";
import { Col } from "antd";

export function PageTitle({ value, name, ...rest }: any) {

  return (
    <Col className="page__title" {...rest}>
      <Title level={2}>{value}</Title>
    </Col>
  );
}
