import React from "react";
import { useEffect, useState } from "react";
import { Button } from "antd";
import { useFormikContext } from "formik";
import { getEventHandler } from "../registries/event.registry";
import getIcon from "../registries/icon.registry";
import useCheckCondition from "../hooks/useCheckCondition";

export function SimpleButton({name, settings }: any) {
  const { text, onClickFn, type = "primary", prefix, disabledIf, ...rest } = settings;
  const formikContext = useFormikContext();

  let onClick = null;
  if (onClickFn) {
    onClick = getEventHandler(onClickFn, formikContext);
  }

  const prefixComp = getIcon(prefix);

  const [checkDisabledIf, setDisabledIf] = useState(false);
  const memoCheckDisabledIf = useCheckCondition(name, disabledIf, formikContext)();

  useEffect(() => {
    setDisabledIf(memoCheckDisabledIf);
    // eslint-disable-next-line
  }, [memoCheckDisabledIf]);

  return (
    <Button
      prefix={prefixComp}
      className="dr_button"
      type={type}
      onClick={onClick}
      disabled={checkDisabledIf}
      {...rest}
    >
      {text}
    </Button>
  );
}
