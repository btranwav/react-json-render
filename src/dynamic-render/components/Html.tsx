import React from "react";

export function Html({ html, value }: any) {
  return (
      <div dangerouslySetInnerHTML={{__html: value || html}} />
  );
}
