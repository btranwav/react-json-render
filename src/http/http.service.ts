export class HttpService {

  static prefix = 'http://localhost:4002/api/e2f-annotation';
  static token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6Njk2OSwiZTJmdHJpZ3JhbSI6IldBQkFUIiwiY29uZmlncyI6eyJhZ2VuY3kiOm51bGwsImN1c3RvbV9hY2Nlc3MiOnsiYWNlUmV2aWV3IjoiQUNFLXZlbmRvcl90cmFpbmluZy0wMDEiLCJicmF0VGVzdGluZyI6IkJSQVQtZW5fVVMtMDAxIiwiY29udmVyc2F0aW9uIjoiU0ktZW5fVVMtMDAxIiwiYWNlQW5ub3RhdGlvbiI6IkFDRS12ZW5kb3JfdHJhaW5pbmctMDAxIiwibmF0dXJhbFJlc3BvbnNlIjoiTlItZW5fVVMtMDAzIiwicmVzcG9uc2VFZGl0aW5nIjoiTlJFLWVuX1VTLTAwMiIsInJlc3BvbnNlRWRpdGluZ1JldmlldyI6Ik5SRS1lbl9VUy0wMDIifX0sInF1ZXN0aW9uX2Fubm90YXRpb25fcmlnaHRzIjoiWC1lbl9VUy0wMDEiLCJhbnN3ZXJfYW5ub3RhdGlvbl9yaWdodHMiOiJYLWVuX1VTLTAwMSIsImFkbWluIjp0cnVlLCJpYXQiOjE2ODk3NTY0NzIsImV4cCI6MTY4OTc1NjUzMn0.sqtH_xChicg6tAPmCTFSHlHEhPco9u_vnoa854fAwkc';

  static async get(url: string) {
    const result = await fetch(this.prefix + '/' + url, {
      method: 'GET',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.token
      }
    });

    return result.json();
  }

  static async post(url: string, data: any) {
    const result = await fetch(this.prefix + '/' + url, {
      method: 'POST',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.token
      },
      body: JSON.stringify(data)
    });

    return result.json();
  }

  static async put(url: string, data: any) {
    const result = await fetch(this.prefix + '/' + url, {
      method: 'POST',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
        'token': this.token
      },
      body: JSON.stringify(data)
    });

    return result.json();
  }
}