declare interface Window {
  fcWidget?: {
      user?: any
  }
  Pusher?: any
}

declare interface MinimalChangeEvent {
  target: {
      value: string
  }
}

declare type Handler<T> = (args: T) => void

declare type PromiseHandler<T> = (args: T) => Promise<void>

declare type TimeOutId = ReturnType<typeof setTimeout>

declare type Fun0 = () => void;
declare type Fun1<T> = (p: T) => void;
declare type Fun2<P0, P1> = (p0: P0, p1: P1) => void;
declare type Fun3<P0, P1, P2> = (p0: P0, p1: P1, p2: P2) => void;

declare interface AnyObject {
  [key: string]: any | undefined;
}
