export const greenfi_theme = {
  "token": {
    "colorPrimary": "#00b96b"
  },
  "components": {
    "Menu": {
      "colorPrimary": "#0A4D23",
      "itemBg": "#01401C",
      "controlItemBgActive": "#0A4D23",
      "colorText": "rgba(255, 255, 255, 0.7)"
    },
    "Button": {
      "colorBorder": "#00b96b",
      "colorLink": "#00b96b",
      "colorText": "#00b96b"
    }
  }
};
