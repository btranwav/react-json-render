import yup from "../../../dynamic-render/registries/validator.registry";

const add_user_modal_schema = {
  uniqueName: "add_user_modal_schema",
  validationSchema: yup.object({
    firstName: yup.string().required(),
  }),
  initialValue: {},
  formSchema: {
    modalTitle: {
      flex: 1,
      component: "title",
      value: "Add new user",
    },
    firstName: {
      component: "text",
      label: "First name",
      required: true,
    },
    lastName: {
      component: "text",
      label: "Last name",
    },
    username: {
      component: "text",
      label: "Username",
    },
    department: {
      component: "text",
      label: "Department",
    },
    email: {
      component: "text",
      label: "Email",
    },
  },
};
 export default add_user_modal_schema;