import { useFormikContext } from "formik";
import DynamicModal from "../../../dynamic-render/core/DynamicModal";
import formConfig from "./add-user-modal.schema";
import events from "./events";

type Props = {
  settings: {
    title: string,
    width: string | number,
    layout: 'vertical' | 'horizontal',
    // formConfig: any,
    // onOk: (ctx: FormikContextType<any>) => (childCtx: FormikContextType<any>) => {},
    // onCancel: (ctx: FormikContextType<any>) => (childCtx: FormikContextType<any>) => {},
  }
}

export default function AddUserModal({ settings }: Props) {
  const formikContext = useFormikContext();

  const showModal = formikContext?.getFieldMeta("showUserModal").value;

  return (
    <DynamicModal
      open={showModal}
      formConfig={formConfig}
      onOk={events.addUser(formikContext)}
      onCancel={events.closeModal(formikContext)}
      {...settings}
    />
  );
}
