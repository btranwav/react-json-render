import { FormikContextType } from "formik";
import { isEmpty } from "lodash";

function addUser(parentContext: FormikContextType<any>) {
  return (formikContext: FormikContextType<any>) => {
    formikContext.validateForm().then((result: any) => {
      formikContext.setTouched(result);

      if (!isEmpty(result)) {
        setTimeout(() => {
          alert("Please filled all required fields");
        }, 500);
      } else {
        alert(JSON.stringify(formikContext.values));
        parentContext.setFieldValue("showUserModal", 0);
        parentContext.setFieldValue("user", formikContext.values)
      }
    });
  };
}

function closeModal(parentContext: FormikContextType<any>) {
  return () => {
    parentContext.setFieldValue("showUserModal", 0);
  };
}


const events = {
  addUser,
  closeModal,
};

export default events;
