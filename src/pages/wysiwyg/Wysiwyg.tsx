import React, { useEffect, useState } from "react";
import DynamicForm from "../../dynamic-render/core/DynamicForm";
import "./styles.scss";
import { Row, Col } from "antd";
import { Button } from "antd/es/radio";
import { Sample } from "./sample";
import JSONEditor from "react-json-editor-ui";
import "reactjs-json-editor/css/style.css";
import { registerEvents } from "../../dynamic-render/registries/event.registry";
import { registerComponent } from "../../dynamic-render/registries/component.registry";
import events from "./events";
import AddUserModal from "./modal/AddUserModal";

export default function Wysiwyg() {
  const [editObject, setEditObject] = React.useState<any>(Sample);
  const [formSchema, setFormSchema] = useState({});
  const [initialData] = useState({ showUserModal: 0 });

  useEffect(() => {
    registerEvents(events);
    registerComponent("addUserModal", AddUserModal, true, true);
    // eslint-disable-next-line
  }, []);

  return (
    <>
      <Row>
        <Row
          style={{
            width: "100%",
            fontSize: 16,
            fontWeight: 500,
            paddingBottom: 20,
          }}
        >
          What you see is what you get
        </Row>
        <Col style={{ height: "600px", width: "560px", overflow: "scroll" }}>
          <JSONEditor
            data={editObject}
            onChange={(evt: any) => setEditObject(evt)}
          />
        </Col>
        <Col flex={"0 1 50%"}>
          <DynamicForm
            id={"wysiwyg"}
            formSchema={formSchema}
            initialValue={initialData}
          />
        </Col>
      </Row>
      <Row>
        <Button
          onClick={(evt) => {
            setFormSchema({ ...editObject });
          }}
        >
          Apply changes
        </Button>
      </Row>
    </>
  );
}
