import { ICON } from "../../dynamic-render/registries/icon.registry";

export const Sample = {
  formTitle: {
    component: "label",
    className: "formTitle",
    style: {
      color: 'red'
    },
    settings: {
      text: "Dynamic form",
      style: {
        fontSize: 20
      }
    },
  },
  wrapperLogin: {
    username: {
      component: "text",
      label: "Username",
    },
    password: {
      component: "password",
      label: "Password",
    },
  },
  country: {
    component: "select",
    label: "Select a country",
    settings: {
      options: [
        {
          label: "Select",
          value: "",
        },
        {
          label: "Vietnam",
          value: "VN",
        },
        {
          label: "USA",
          value: "US",
        },
      ],
    },
  },
  state: {
    component: "select",
    label: "Select a state",
    settings: {
      optionfn: {
        dependentFields: ["country"],
        funcName: "getStates",
      },
    },
  },
  city: {
    component: "select",
    label: "Select a city",
    settings: {
      optionfn: {
        dependentFields: ["country", "state"],
        funcName: "getCities",
      },
    },
  },
  wrapperSearch: {
    justify: "space-between",
    keywords: {
      component: "text",
      flex: 1,
      settings: {
        placeholder: "Enter keywords",
        prefix: ICON.SearchOutlined,
      },
    },
    btnSearch: {
      component: "button",
      style: {
        width: "fit-content",
      },
      settings: {
        text: "Search",
        onClickFn: "showData",
        disabledIf: {
          field: "keywords",
          op: "isEmpty",
          value: "",
        },
      },
    },
  },
  btnOpenDialog: {
    component: "iconButton",
    style: {
      width: "fit-content",
    },
    settings: {
      text: "Add new user",
      icon: "UserOutlined",
      onClickFn: "onClickAddBtn",
    },
  },
  dialog: {
    addUserModal: {
      component: "addUserModal",
      isModal: true,
      settings: {
        title: "Add new user",
        onOkFn: 'addUser',
        onCancelFn: 'closeModal',
        width: "550px",
        okText: "Add new",
      }
    },
  },
};
