import { State, City } from "country-state-city";
import { FormikContextType } from "formik";
import { isEmpty } from "lodash";

function getStates(formikContext: FormikContextType<any>) {
  return (dependentValues: string[], currentName: string) => {
    try {
      if (formikContext.values) {
        formikContext?.setFieldValue(currentName, "", true);
      }
    } catch {}

    if (isEmpty(dependentValues)) {
      return Promise.resolve([]);
    }

    const states = State.getStatesOfCountry(dependentValues[0]);
    return Promise.resolve(
      states.map((s: any) => ({
        label: s.name,
        value: s.isoCode,
      }))
    );
  };
}

function getCities(formikContext: FormikContextType<any>) {
  return (dependentValues: string[], currentName: string) => {
    try {
      if (formikContext.values) {
        formikContext.setFieldValue(currentName, "", true);
      }
    } catch {}

    const cities = City.getCitiesOfState(
      dependentValues[0],
      dependentValues[1]
    );
    return Promise.resolve(
      cities.map((c: any) => ({
        label: c.name,
        value: c.name,
      }))
    );
  };
}

function showData(formikContext: FormikContextType<any>) {
  return () => {
    formikContext.validateForm().then((result: any) => {
      formikContext.setTouched(result);

      if (!isEmpty(result)) {
        setTimeout(() => {
          formikContext.setFieldValue("isLoading", false, false);
          alert("Please filled all required fields");
        }, 500);
      } else {
        alert(JSON.stringify(formikContext.values));
      }
    });
  };
}

function onClickAddBtn(formikContext: FormikContextType<any>) {
  return () => {
    formikContext.setFieldValue("showUserModal", 1);
  };
}

const events = {
  getStates,
  getCities,
  showData,

  onClickAddBtn,
};

export default events;
