import { FormikContextType } from "formik";
import { isEmpty } from "lodash";

function getSelectOptions() {
  return () => Promise.resolve([
    { label: "", value: '' },
    { label: "wav", value: 'wav' },
    { label: "mp3", value: 'mp3' },
    { label: "mp4", value: 'mp4' },
  ]);
}

function getSelectOptionsV2() {
  return (dep: string[] = []) => Promise.resolve([
    { label: "", value: '' },
    { label: "wav", value: 'wav' },
    { label: "mp3", value: 'mp3' },
    { label: "mp4", value: 'mp4' },
  ].filter(o => !dep.includes(o.value)));
}

function getRememberOption() {
  return () => Promise.resolve([
    { label: "Remember me", value: 'true' },
  ]);
}

function onButtonClicked(formikContext: FormikContextType<any>, history: History) {
  return () => {
    formikContext.setFieldValue('isLoading', true)
    setTimeout(() => {
      formikContext.setFieldValue('isLoading', false);
      console.log('Form values: ', formikContext.values);
    }, 3000);    
  }
}

function onValidateClicked(formikContext: FormikContextType<any>, history: History) {
  return async () => {
    const errors = await formikContext.validateForm();
    if(isEmpty(errors)) {
      console.log('Form values: ', formikContext.values);
    } else {
      console.log('Errors: ', errors);
    }
  }
}

export const TestComponentEvents = {
  getSelectOptions,
  getSelectOptionsV2,
  getRememberOption,
  onButtonClicked,
  onValidateClicked
};