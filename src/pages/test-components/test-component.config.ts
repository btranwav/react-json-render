import { isEmpty } from "lodash";
import * as yup from "yup";
import { Op } from "../../dynamic-render/types/operator";

export const testComponentConfig = {
  uniqueName: 'testComponent',
  validationSchema: yup.object({
    username: yup.string().when('cbValue', {
      is: (cbValue: string[]) => !isEmpty(cbValue),
      then: yup.string().required()
    }).max(10)
  }),
  initialValue: {
    cbValue: [],
    radioValue: 'mp3',
    selectValue: 'mp4',
    multiSelectValue: ['wav', 'mp4'],
    username: 'my user name',
    rememberMe: 'true',
    datePicker: '2021-11-23T15:34:04.425Z'
  },
  formSchema: {
    pageTitle: {
      // flex: 1,
      component: 'title',
      title: 'Dynamic render',
      subTitle: 'Component showcases'
    },
    wrapperForm: {
      style: {
        justifyContent: 'space-between'
      },
      wrapperRow1: {
        // flex: 1,
        style: {
          justifyContent: 'space-between'
        },
        cbValue: {
          // flex: 1,
          component: 'checkboxes',
          label: "Ant checkbox group",
          options: [
            { label: "wav", value: 'wav' },
            { label: "mp3", value: 'mp3' },
            { label: "mp4", value: 'mp4' },
          ]
        },
        radioValue: {
          // flex: 1,
          component: 'radio',
          label: "Ant radio group",
          options: [
            { label: "wav", value: 'wav' },
            { label: "mp3", value: 'mp3' },
            { label: "mp4", value: 'mp4' },
          ]
        },
      },
      wrapperRow2: {
        // flex: 1,
        selectValue: {
          // flex: 1,
          component: 'select',
          placeholder: 'Please select a value',
          label: 'Ant select',
          optionfn: {
            funcName: 'getSelectOptions',
            dependentFields: []
          },
          disabledIf: {
            field: 'cbValue',
            value: 'wav',
            op: Op.includes
          }
        },
        // emptyBlock: {
        //   component: 'empty',
        //   // flex: 1,
        //   onlyDisplay: true
        // },
        selectValueV2: {
          // flex: 1,
          className: 'my-class',
          component: 'selectV2',
          label: 'Ant select',
          disabledIf: {
            field: 'cbValue',
            value: 'wav',
            op: Op.includes
          },
          settings: {
            placeholder: 'Please select a value',
            options: 'getSelectOptionsV2',
            dependents: ['selectValue'],
          }
        },
      },

      multiSelectValue: {
        // flex: '40%',
        component: 'select',
        mode: 'multiple',
        placeholder: 'Please select multi values',
        label: 'Multi select',
        optionfn: {
          funcName: 'getSelectOptions',
          dependentFields: []
        }
      },
      username: {
        // flex: '20%',
        component: 'text',
        label: 'Username',
        placeholder: 'Enter your email or phone number',
        requiredIf: {
          field: 'cbValue',
          value: 'mp3',
          op: Op.includes
        },
        disabledIf: {
          field: 'cbValue',
          value: 'wav',
          op: Op.includes
        }
      },
    },
    wrapper2: {
      style: {
        justifyContent: 'space-between'
      },
      rememberMe: {
        component: 'checkboxes',
        // flex: 1,
        optionfn: {
          funcName: 'getRememberOption',
          depentFields: []
        }
      },
      datePicker: {
        // flex: 1,
        component: 'datePicker',
        label: 'Date picker',
        settings: {
          style: {
            justifyContent: 'space-between'
          },
        }
      },
    },
    wrapperButtons: {
      submit: {
        flex: '20%',
        component: 'button',
        settings: {
          text: 'Submit',
          onClickFn: 'onButtonClicked',
          className: 'my-button'
        }
      },
      validateButton: {
        flex: '20%',
        component: 'button',
        className: 'validate-button',
        settings: {
          text: 'Validate',
          onClickFn: 'onValidateClicked',
          className: 'my-button'
        }
      }
    }
  }
};
