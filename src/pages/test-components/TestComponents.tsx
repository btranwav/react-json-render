import React, { useEffect, useState } from "react";
import { testComponentConfig } from "./test-component.config";
import DynamicForm from "../../dynamic-render/core/DynamicForm";
import { registerEvents } from "../../dynamic-render/registries/event.registry";
import { TestComponentEvents } from "./events";
import useLoading from "../../dynamic-render/hooks/useLoading";
import { validateData } from "../../dynamic-render/validate-data";

export default function TestComponents() {
  const [formSchema, setFormSchema] = useState({});
  const [validationSchema, setValidationSchema] = useState(null);
  const formConfig = useLoading(testComponentConfig, false);
  const [initialValue, setInitialValue] = useState(formConfig.initialValue);


  useEffect(() => {
    registerEvents(TestComponentEvents);
    const temp = initialValue;
    validateData(formConfig.formSchema, temp)
    setInitialValue(temp);
    setValidationSchema(formConfig.validationSchema);
    setFormSchema(formConfig.formSchema);

    // eslint-disable-next-line
  }, []);

  return (
    <DynamicForm
      formSchema={formSchema}
      initialValue={initialValue}
      validationSchema={validationSchema}
      layout='horizontal'
    />
  );
}
