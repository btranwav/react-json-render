import { FormikContextType } from "formik";
import { get, isEmpty } from "lodash";
import { ConversationApi } from "./services";
import { getFieldByIndex } from "../../dynamic-render/core/utils";

function submitConversationAnnotationAndNext(
  formikContext: FormikContextType<any>
) {
  // args[0] is formik context
  return async () => {
    const result = await submit(formikContext, true);

    if (result) {
      window.location.href = "";
    }
  };
}

function saveConversationAnnotation(...args: any[]) {
  // args[0] is formik context
  return async () => {
    const formikContext = args[0] as FormikContextType<any>;
    const result = await submit(formikContext, false, true);
    if (result) {
      alert("Saved");
    }
  };
}

async function submit(
  formikContext: FormikContextType<any>,
  returnNext: boolean,
  save = false
) {
  const { getFieldMeta, setFieldValue } = formikContext;
  const conversationAnnotation: any = getFieldMeta("annotation").value;
  const conversationId = getFieldMeta<string>(
    "conversation.conversationId"
  ).value;

  const result: any = await formikContext.validateForm();
  formikContext.setTouched(result);
  if (isEmpty(result)) {
    setFieldValue("isLoading", true);

    const request = {
      conversationId,
      conversationAnnotation: {
        issues: conversationAnnotation.issues,
        intentSlots: conversationAnnotation.intentSlots.map((el: any) => ({
          intent: el.intent,
          slots: el.slots,
        })),
        slots: conversationAnnotation.slots,
        comment: conversationAnnotation.comment,
      },
      returnNext,
      save,
    };
    return ConversationApi.submitConversationAnnotation(request).then(
      (data: any) => {
        setFieldValue("isLoading", false);
        return data;
      }
    );
  }

  return Promise.resolve(null);
}

function getCommentOptions() {
  return async () => {
    return Promise.resolve([
      {
        value: "Positive",
        label: "Positive",
      },
      {
        value: "Negative",
        label: "Negative",
      },
      {
        value: "Neutral",
        label: "Neutral",
      },
    ]);
  };
}

function addIntentSlots(formikContext: FormikContextType<any>) {
  return () => {
    const { getFieldMeta, setFieldValue } = formikContext;
    const intentSlots = getFieldMeta<any[]>("annotation.intentSlots").value;
    intentSlots.push({
      intent: [],
      slots: [],
    });
    setFieldValue("hideRemoveIntentSlotsButton", false);
  };
}

function removeIntentSlots(formikContext: FormikContextType<any>) {
  return () => {
    const { getFieldMeta, setFieldValue } = formikContext;
    const intentSlots = getFieldMeta<any[]>("annotation.intentSlots").value;
    intentSlots.pop();
    if (intentSlots.length < 2) {
      setFieldValue("hideRemoveIntentSlotsButton", true);
    } else {
      // dirty: need this line to trigger change to formik
      setFieldValue("hideRemoveIntentSlotsButton", false);
    }
  };
}

function onIntentChange(
  formikContext: FormikContextType<any>,
  { currentFieldName }: any
) {
  return () => {
    const fieldName = getFieldByIndex('annotation.intentSlots.$index.slots', currentFieldName);
    formikContext.setFieldValue(fieldName, []);
  };
}

function getSlotSuggestions(formikContext: FormikContextType<any>) {
  return async (dependentValues: any[]) => {
    console.log({ dependentValues });
    if (get(dependentValues, 0, []).includes("apply coupon")) {
      return Promise.resolve([
        { value: "name", label: "name" },
        { value: "phone number", label: "phone number" },
        { value: "email", label: "email" },
        { value: "account number", label: "account number" },
      ]);
    }
    return Promise.resolve([
      { value: "name", label: "name" },
      { value: "phone number", label: "phone number" },
      { value: "email", label: "email" },
      { value: "account number", label: "account number" },
      { value: "date of birth", label: "date of birth" },
      { value: "SSN", label: "SSN" },
      { value: "age", label: "age" },
      { value: "shipping preference", label: "shipping preference" },
      { value: "order number", label: "order number" },
      { value: "product name", label: "product name" },
      { value: "billing address", label: "billing address" },
      { value: "payment method", label: "payment method" },
      { value: "credit card number", label: "credit card number" },
    ]);
  };
}

function getIntentSuggestions() {
  return async (dependentValues: any[]) => {
    return Promise.resolve([
      { label: "apply coupon", value: "apply coupon" },
      { label: "check order status", value: "check order status" },
      { label: "change an order", value: "change an order" },
      { label: "make an exchange", value: "make an exchange" },
      { label: "make a return", value: "make a return" },
      { label: "place an order", value: "place an order" },
      { label: "track an order", value: "track an order" },
      { label: "ask FAQ", value: "ask FAQ" },
      { label: "get gift card info", value: "get gift card info" },
      { label: "get pricing info", value: "get pricing info" },
      { label: "get product info", value: "get product info" },
      { label: "get store hours", value: "get store hours" },
      { label: "activate account", value: "activate account" },
      {
        label: "change account information",
        value: "change account information",
      },
      { label: "close account", value: "close account" },
      { label: "open account", value: "open account" },
      { label: "report fraud", value: "report fraud" },
      {
        label: "report an issue with a delivery",
        value: "report an issue with a delivery",
      },
      { label: "report issue with refund", value: "report issue with refund" },
      { label: "report issue with order", value: "report issue with order" },
      { label: "make a complaint", value: "make a complaint" },
      { label: "request callback", value: "request callback" },
    ]);
  };
}

function getIssueOptions() {
  return () =>
    Promise.resolve([
      {
        value: "incorrectSpeakerRole",
        label: "Incorrect speaker roles",
      },
      {
        value: "multipleConversation",
        label: "Multiple conversations",
      },
      {
        value: "unfinishedConversation",
        label: "Unfinished conversation",
      },
      {
        value: "unredacted",
        label: "Unredacted sensitive PII",
      },
      {
        value: "none",
        label: "None",
      },
    ]);
}

export const ConversationAnnotationEvents = {
  getSlotSuggestions,
  getIntentSuggestions,
  removeIntentSlots,
  addIntentSlots,
  getCommentOptions,
  submitConversationAnnotationAndNext,
  saveConversationAnnotation,
  getIssueOptions,
  onIntentChange
};
