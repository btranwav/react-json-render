import React, { useEffect, useState } from "react";
import { formConfig } from "./form.config";
import DynamicForm from "../../dynamic-render/core/DynamicForm";
import { registerEvents } from "../../dynamic-render/registries/event.registry";
import useLoading from "../../dynamic-render/hooks/useLoading";
import { validateData } from "../../dynamic-render/validate-data";
import { ConversationAnnotationEvents } from "./events";
import './styles.scss';
import { ConversationApi } from "./services";

export default function ConversationAnnotation() {
  const [formSchema, setFormSchema] = useState({});
  const [validationSchema, setValidationSchema] = useState(null);
  const [initialValue, setInitialValue] = useState({});

  const _formConfig = useLoading(formConfig, false);
  
  useEffect(() => {
    registerEvents(ConversationAnnotationEvents);
    setValidationSchema(_formConfig.validationSchema);
      setFormSchema(_formConfig.formSchema);
    ConversationApi.getConversation('SI-en_US-001').then((result) => {
      validateData(_formConfig.formSchema, result)
      setInitialValue(result);
      
    });

    // eslint-disable-next-line
  }, []);

  return (
    <DynamicForm
      id={formConfig.uniqueName}
      formSchema={formSchema}
      initialValue={initialValue}
      validationSchema={validationSchema}
    />
  );
}
