import { compact, flattenDeep } from "lodash";
import yup from "../../dynamic-render/registries/validator.registry";
import { Op } from "../../dynamic-render/types/operator";

type IntentSlots = {
  intent: string[];
  slots: string[];
};

yup.addMethod(yup.array, "uniqueIntent", function (this: any, message: string) {
  return this.test(
    "uniqueIntent",
    message,
    function (list: IntentSlots[] = []) {
      const intents = flattenDeep(
        list.map((el) => (el.intent || []))
      );

      const uniqueIntents: string[] = [];
      let duplicated = false;
      compact(intents).forEach((i) => {
        if (uniqueIntents.includes(i)) {
          duplicated = true;
        } else {
          uniqueIntents.push(i);
        }
      });

      return !duplicated;
    }
  );
});

export const formConfig = {
  uniqueName: "conversation",
  validationSchema: yup.object({
    annotation: yup.object({
      issues: yup.array().ensure().min(1, "Required"),
      intentSlots: yup
        .array()
        .of(
          yup.object({
            intent: yup.array().ensure().min(1, "Required"),
            slots: yup.array().ensure().min(1, "Required"),
          })
        )
        .uniqueIntent("Duplicated intents"),
      comment: yup.string().required("Required"),
    }),
  }),
  initialValue: { hideRemoveIntentSlotsButton: true },
  formSchema: {
    wrapperHeader: {
      wrapperTitle: {
        flex: 1,
        // style: {
        //   width: "58%",
        // },
        header: {
          component: "label",
          // onlyDisplay: true,
          settings: {
            text: "Conversation Annotation",
            style: {
              fontWeight: 600,
              fontSize: 16,
              paddingTop: 5,
            },
          },
        },
      },
      wrapperButtons: {
        
        style: {
          width: "fit-content",
          marginBottom: 15,
        },
        submitLogout: {
          flex: 1,
          component: "button",
          // onlyDisplay: true,
          settings: {
            text: "Submit and Quit",
            onClickFn: "submitConversationAnnotation",
            style: {
              marginTop: 10,
              backgroundColor: "#f1f1f1",
              color: "black",
              border: "1px solid #d1d1d1",
            },
          },
        },
        submitNext: {
          component: "button",
          style: {
            width: "fit-content",
          },
          settings: {
            text: "Submit and Continue",
            onClickFn: "submitConversationAnnotationAndNext",
            style: {
              float: "right",
              marginTop: 10,
            },
          },
        },
      },
    },
    wrapperConversation: {
      conversation: {
        flex: 3,
        style: {
          border: "1px solid gray",
          padding: 7,
          overflowY: "scroll",
          maxHeight: 550,
        },
        conversationId: {
          component: "title",
          // onlyDisplay: true,
          settings: {
            text: "Conversation ID:",
            style: {
              fontSize: 14,
              fontWeight: 600,
            },
          },
        },
        speakers: [
          {
            speaker: {
              component: "html",
              // onlyDisplay: true,
              settings: {
                html: "<b>agent</b>",
              },
            },
          },
        ],
      },
      annotation: {
        flex: 2,
        style: {
          // paddingLeft: 100,
          alignContent: 'flex-start',
          border: "1px solid gray",
          padding: 7,
          overflowY: "scroll",
          // maxHeight: 550,
          // marginLeft: 15,
        },
        issues: {
          component: "checkboxes",
          label: "Annotate Top-level Issues",
          settings: {
            optionfn: {
              funcName: "getIssueOptions",
              dependentFields: [],
            },
          },
        },
        intentSlots: [
          {
            wrapperIntentSlots: {
              style: {
                justifyContent: "space-between",
              },
              intent: {
                flex: 1,
                component: "select",
                label: "Intents",
                settings: {
                  mode: "tags",
                  maxLength: 1,
                  maxTagCount: 100,
                  placeholder: "single phrase/sentence",
                  optionfn: {
                    funcName: "getIntentSuggestions",
                    dependentFields: [],
                  },
                  onChangeFn: 'onIntentChange'
                },
              },
              slots: {
                flex: 1,
                component: "select",
                label: "Slots",
                settings: {
                  mode: "tags",
                  maxLength: 1,
                  maxTagCount: 100,
                  placeholder: "multiple phrases/sentences",
                  optionfn: {
                    funcName: "getSlotSuggestions",
                    dependentFields: ['annotation.intentSlots.$index.intent'],
                  },
                },
              },
            },
          },
        ],
        wrapperIntentButtons: {
          style: {
            justifyContent: "space-between",
            flexDirection: "row-reverse",
            width: "95%",
            marginBottom: "10px",
          },
          addMoreButton: {
            flex: 1,
            component: "button",
            settings: {
              text: "Add more",
              onClickFn: "addIntentSlots",
            },
          },
          removeIntentSlotsButton: {
            flex: 1,
            component: "button",
            hiddenIf: {
              field: "hideRemoveIntentSlotsButton",
              value: true,
              op: Op.equals,
            },
            settings: {
              text: "Remove",
              onClickFn: "removeIntentSlots",
              
            },
          },
        },

        comment: {
          component: "radio",
          label: "Comment",
          settings: {
            optionfn: {
              funcName: "getCommentOptions",
              dependentFields: [],
            },
          },
        },
        wrapperBottomButtons: {
          style: {
            justifyContent: "space-between",
          },
          submitLogout: {
            // flex: 1,
            style: {
              width: 'fit-content'
            },
            component: "button",
            settings: {
              text: "Submit and Quit",
              onClickFn: "submitConversationAnnotation",
              style: {
                backgroundColor: "#f1f1f1",
                color: "black",
                border: "1px solid #d1d1d1",
              },
            },
          },
          submitNext: {
            component: "button",
            style: {
              width: 'fit-content'
            },
            settings: {
              text: "Submit and Continue",
              onClickFn: "submitConversationAnnotationAndNext",
            },
          },
        },
      },
    },
  },
};
