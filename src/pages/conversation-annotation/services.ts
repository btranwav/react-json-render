// import { HttpService } from "../../http/http.service";

const mockData = {
  "id": "00ef36a1-0bc5-5ac9-ac7b-735b3a76c491",
  "projectName": "R-en_US-005-TEST",
  "conversation": [
      {
          "start": 2.933333333333333,
          "content": "Thank you for [noise] calling. My name is [redacted-name-first]. How can I [noise] assist you?",
          "speaker": "Agent"
      },
      {
          "start": 6.333333333333333,
          "content": "[noise] Yes. [no-speech] [noise] My niece and I, my niece buys [redacted-other] for me. [noise] [no-speech] and [no-speech] according to what I saw on your website,",
          "speaker": "Agent"
      },
      {
          "start": 18.133333333333333,
          "content": "in her [noise] when she accessed #er accessed hers, [noise] (()) pasta was no longer available. [noise] What's going on with that? [noise]",
          "speaker": "Agent"
      },
      {
          "start": 29.566666666666663,
          "content": "In order to help her, please provide [noise] me the email, I'm sorry, the [noise] the name and the phone number on the account.",
          "speaker": "Agent"
      },
      {
          "start": 37.93333333333333,
          "content": "#ah I think it's my phone number, [redacted-phone]. [noise]",
          "speaker": "Agent"
      },
      {
          "start": 45.7,
          "content": "[noise] And your name is?",
          "speaker": "Agent"
      },
      {
          "start": 49.43333333333333,
          "content": "[noise] OK. Thank you. [noise]",
          "speaker": "Agent"
      },
      {
          "start": 57.56666666666666,
          "content": "[sta] OK, so you want to know why you paid more for this coming order, correct?",
          "speaker": "Agent"
      },
      {
          "start": 63.5,
          "content": "No. No. [noise] [other-speech] I was wondering why [noise] OK, this next order for the week of the [redacted-date-time], [noise] according to what I saw on my phone, [noise] [no-speech]",
          "speaker": "Agent"
      },
      {
          "start": 75.73333333333332,
          "content": "the squid and pasta was available, but then when I had my niece order it, [noise] [no-speech] it's like I texted her with what I wanted [noise] cause she's the one that orders it for me, [noise]",
          "speaker": "Agent"
      },
      {
          "start": 87.46666666666665,
          "content": "mhm",
          "speaker": "Agent"
      },
      {
          "start": 88.06666666666665,
          "content": "[noise] #um [noise] she says she could not find the squid and pasta at all.",
          "speaker": "Agent"
      },
      {
          "start": 93.1,
          "content": "[sta] [noise] When that happen is because the order, the m~ the meal is already sold out. Let me <overlap>confirm.</overlap> <overlap>mhm</overlap>",
          "speaker": "Agent"
      },
      {
          "start": 99.83333333333331,
          "content": "<overlap>Sold out.</overlap>",
          "speaker": "Agent"
      },
      {
          "start": 101.13333333333333,
          "content": "<overlap>OK.</overlap>",
          "speaker": "Agent"
      },
      {
          "start": 101.93333333333332,
          "content": "[sta] Thank you. Because wha~ but I~ I get it like in an hour before. [noise]",
          "speaker": "Agent"
      },
      {
          "start": 107.03333333333332,
          "content": "[sta] <overlap>Yes,</overlap> that #hm that <overlap>can happen</overlap> really quickly. [breath] But I advise you to, it's because you have more time, [breath] you can #um check again with mm maybe tomorrow.",
          "speaker": "Agent"
      },
      {
          "start": 107.79999999999998,
          "content": "<overlap>OK.</overlap>",
          "speaker": "Agent"
      },
      {
          "start": 109.16666666666666,
          "content": "<overlap>(()).</overlap>",
          "speaker": "Agent"
      },
      {
          "start": 119.4,
          "content": "[breath] Sometimes when customers #um [noise] cancel orders, [breath] the meals can have #ah can get available again. You have, you sti~ you still have time [breath] <overlap>until</overlap> [noise]",
          "speaker": "Agent"
      },
      {
          "start": 129.43333333333334,
          "content": "<overlap>OK.</overlap>",
          "speaker": "Agent"
      },
      {
          "start": 130.7,
          "content": "[noise] until tomorrow [noise] midnight to do any changes for [breath] #ah [redacted-date-time].",
          "speaker": "Agent"
      },
      {
          "start": 137.6,
          "content": "OK. [noise] [no-speech]",
          "speaker": "Agent"
      },
      {
          "start": 139.5,
          "content": "OK? [breath]",
          "speaker": "Agent"
      },
      {
          "start": 140.66666666666666,
          "content": "[sta] Thank you.",
          "speaker": "Agent"
      },
      {
          "start": 141.6,
          "content": "Is [breath] you're welcome. Is there [noise] anything else I can help you with?",
          "speaker": "Agent"
      },
      {
          "start": 145.03333333333333,
          "content": "[sta] No.",
          "speaker": "Agent"
      },
      {
          "start": 145.9,
          "content": "[noise] OK. Have a nice rest of your day.",
          "speaker": "Agent"
      },
      {
          "start": 148.56666666666666,
          "content": "[noise] I will.",
          "speaker": "Agent"
      },
      {
          "start": 150.73333333333332,
          "content": "Bye-bye. [sta]",
          "speaker": "Agent"
      }
  ],
  "annotatorId": 6969,
  "reviewerId": null,
  "annotation": {
      "issues": [
          "unredacted",
          "unfinishedConversation"
      ],
      "comment": "Negative",
      "intentSlots": [
          {
              "slots": [
                  "name",
                  "email"
              ],
              "intent": [
                  "make an exchange"
              ]
          },
          {
              "slots": [
                  "name",
                  "account number"
              ],
              "intent": [
                  "apply coupon",
                  "change an order"
              ]
          }
      ]
  },
  "status": "Annotated",
  "comment": null,
  "metadata": null,
  "createdAt": "2021-11-08T06:57:38.000Z",
  "updatedAt": "2021-11-08T07:06:35.000Z"
};

export class ConversationApi {
  static async getConversation(projectName: string): Promise<any> {
    const data = await Promise.resolve(mockData);
    return {
      conversation: {
        conversationId: data.id,
        speakers: data.conversation.map((c: any) => ({
          speaker: `<b>[${c.speaker}]:</b> ${c.content}`
        }))
      },
      annotation: data.annotation
    };
  }

  static submitConversationAnnotation (request: any) {
    return Promise.resolve(true);
  }

  static async getPermissions() {
    return true;
  }
}