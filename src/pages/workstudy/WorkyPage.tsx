import { useEffect, useState } from "react";
import DynamicForm from "../../dynamic-render/core/DynamicForm";
import useLoading from "../../dynamic-render/hooks/useLoading";
import { registerComponents } from "../../dynamic-render/registries/component.registry";
import { registerEvents } from "../../dynamic-render/registries/event.registry";
import { FormConfig } from "../../dynamic-render/types/form-config";
import { validateData } from "../../dynamic-render/validate-data";
import Grades from "./components/Grades";
import { GreenButton } from "./components/GreenButton";
import SignUpButton from "./components/SignupButton";
import { WorkyTextbox } from "./components/WorkyTextbox";
import signup_email_events from "./signup-with-email/signup-email.events";
import signup_events from "./signup/signup.events";
import import_classes from "./student/class/import-classes.events";
import TableEvents from "./student/class/table.events";
import setup_class_events from './class/events';

const initialSchema: FormConfig = {
  uniqueName: "undefined",
  validationSchema: null,
  initialValue: {},
  formSchema: {},
};

const WorkyPage = ({ schema = initialSchema }: any) => {
  registerComponents([
    { name: "grades", component: Grades, onlyDisplay: false },
    {
      name: "worky_text",
      component: WorkyTextbox,
      onlyDisplay: false,
      overwrite: true,
    },
    {
      name: "greenButton",
      component: GreenButton,
      onlyDisplay: true,
      overwrite: true,
    },
    {
      name: "signupButton",
      component: SignUpButton,
      onlyDisplay: true,
      overwrite: true,
    },
  ]);
  registerEvents({
    ...signup_email_events,
    ...signup_events,
    ...import_classes,
    ...TableEvents,
    ...setup_class_events
  });

  const [formSchema, setFormSchema] = useState({});
  const [validationSchema, setValidationSchema] = useState(null);
  const formConfig = useLoading(schema, false);
  const [initialValue, setInitialValue] = useState();

  useEffect(() => {
    let temp = formConfig.initialValue;
    validateData(formConfig.formSchema, temp);
    setInitialValue(temp);
    setValidationSchema(formConfig.validationSchema);
    setFormSchema(formConfig.formSchema);
    // eslint-disable-next-line
  }, [schema]);

  return (
    <DynamicForm
      id={schema.uniqueName}
      formSchema={formSchema}
      initialValue={initialValue}
      validationSchema={validationSchema}
      // layout='horizontal'
    />
  );
};

export default WorkyPage;
