function cleverSignup() {
    return () => {
        window.location.href = 'https://clever.com/oauth/authorize?response_type=code&redirect_uri=http://localhost:3001/workstudy/clever-callback&client_id=83e13575eeec4d4ddde6'
    }
}

function googleSignup() {
    return () => {

    }
}

function classLinkSignup() {
    return () => {

    }
}

function manualSignup() {
    return () => {

    }
}

const signup_events = {
    classLinkSignup,
    cleverSignup,
    googleSignup,
    manualSignup
};

export default signup_events;