
export const signup_screen = {
    uniqueName: 'worky_signup_screen',
    validationSchema: null,
    initialValue: {},
    formSchema: {
        pageTitle: {
            component: 'title',
            value: 'Sign-up',
        },
        hr: {
            component: 'hr'
        },
        subTitle: {
            component: 'subTitle',
            value: 'You are signing up as a teacher'
        },
        wrapperForm: {
            className: 'vertical_form',
            clever: {
                component: 'signupButton',
                settings: {
                    text: "Sign-up with Clever",
                    imgSource: 'https://media.glassdoor.com/sqll/766297/clever-squarelogo-1571854281108.png',
                    onClickFn: 'cleverSignup',
                    className: 'signup_button'
                }
            },
            google: {
                component: 'signupButton',
                settings: {
                    text: "Sign-up with Google Class",
                    imgSource: 'https://upload.wikimedia.org/wikipedia/commons/5/59/Google_Classroom_Logo.png',
                    onClickFn: 'googleSignup',
                    className: 'signup_button'
                }
            },
            classLink: {
                component: 'signupButton',
                settings: {
                    text: "Sign-up with ClassLink",
                    imgSource: 'https://cdn2.downdetector.com/static/uploads/logo/classlink-log.png',
                    onClickFn: 'classLinkSignup',
                    className: 'signup_button'
                }
            }
        },
        link: {
            component: 'link',
            settings: {
                text: 'Not using a LMS?',
                onClickFn: 'manualSignup'
            }
        }
    }
};
