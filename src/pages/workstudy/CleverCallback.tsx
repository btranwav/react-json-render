import { useEffect } from "react";
import { useSearchParams } from "react-router-dom";

// https://clever.com/oauth/authorize?
// channel=clever
// &client_id=4c63c1cf623dce82caac
// &confirmed=true
// &district_id=55f2203f44314a01000048e1
// &redirect_uri=https%3A%2F%2Fclever.com%2Fin%2Fauth_callback
// &response_type=code
// &state=6846c16e390c8e68a723bae70d2f1e8c626d409f15a7585b042c8727839cdeb1

const CleverCallback = () => {
    const [searchParams] = useSearchParams();
    
    
    useEffect(() => {
        // eslint-disable-next-line
        const authCode = window.btoa('83e13575eeec4d4ddde6' + ':' + '45167e13ee60456733692dca492d21e9a8f9218b');
        const code = searchParams.get('code');
        fetch('https://clever.com/oauth/tokens', {
            method: 'POST',
            // mode: 'no-cors',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Basic ' + authCode
            },
            body: JSON.stringify({ code, grant_type: 'authorization_code', redirect_uri: 'http://localhost:3001/workstudy/clever-callback' })
        }).then((result: any) => {
            console.log(result);
        })
    }, [searchParams]);

    return (
        <>Loading</>
    );
}

export default CleverCallback;