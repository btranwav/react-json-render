import { Country, State, City } from "country-state-city";
import { FormikContextType } from "formik";
import { isEmpty } from "lodash";

const countryCode = "US";

function getStates() {
  return () => {
    const country = Country.getCountryByCode(countryCode);
    const states = State.getStatesOfCountry(country.isoCode);
    return Promise.resolve(
      states.map((s: any) => ({
        label: s.name,
        value: s.isoCode,
      }))
    );
  };
}

function getCities(formikContext: FormikContextType<any>) {
  return (dependentValues: string[], currentName: string) => {
    try {
      if (formikContext.values) {
        formikContext.setFieldValue(currentName, "", true);
        formikContext.validate(currentName, "", true);
      }
    } catch {}

    const cities = City.getCitiesOfState(countryCode, dependentValues[0]);
    return Promise.resolve(
      cities.map((c: any) => ({
        label: c.name,
        value: c.name,
      }))
    );
  };
}

function showData(formikContext: FormikContextType<any>) {
  return () => {
    formikContext.validateForm().then((result: any) => {
      formikContext.setTouched(result);

      if (!isEmpty(result)) {
        setTimeout(() => {
          formikContext.setFieldValue("isLoading", false, false);
          alert("Please filled all required fields");
        }, 500);
      } else {
        alert(JSON.stringify(formikContext.values));
      }
    });
  };
}

const signup_email_events = {
  getStates,
  getCities,
  showData,
};

export default signup_email_events;
