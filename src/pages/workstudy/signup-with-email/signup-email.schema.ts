import yup from "../../../dynamic-render/registries/validator.registry";

yup.addMethod(yup.string, "isPassword", function(this: any, fields: any, message: string) {
    return this.test({
        name: 'isPassword', 
        message,
        exclusive: true,
        test: function() {
            // console.log({test: this});
            return false;
        }
    })
})

export const signup_email_screen = {
    uniqueName: 'signup_email_screen',
    validationSchema: yup.object({
        name: yup.string().required(),
        // email: yup.string().required(),
        password: yup.string().required(),
        grades: yup.array().of(yup.string()).min(1),
        location: yup.object().shape({
            state: yup.string().required(),
            city: yup.string().required()
        }),
    }),
    initialValue: {},
    formSchema: {
        pageTitle: {
            component: 'title',
            value: 'Sign-up with Email',
            // subTitle: 'Component showcases'
        },
        hr: {
            component: 'hr'
        },
        wrapperForm: {
            className: 'vertical_form',
            name: {
                component: 'worky_text',
                settings: {
                    placeholder: 'Name',
                }
            },
            email: {
                component: 'worky_text',
                validate: 'validateEmail',
                settings: {
                    placeholder: 'Email',
                    type: 'email',
                }
            },
            password: {
                component: 'worky_text',
                settings: {
                    placeholder: 'Password',
                    type: 'password'
                }
            },
            confirmPassword: {
                component: 'worky_text',
                settings: {
                    placeholder: 'Confirm Password',
                    type: 'password'
                }
            },
            grades: {
                component: 'grades',
                settings: {
                    text: "Grades you are interested in:",
                    tags: ['K', '1', '2', '3', '4', '5', '5+']
                }
            },
            schoolName: {
                component: 'worky_text',
                settings: {
                    placeholder: "School Name",
                }
            },
            location: {
                className: 'city_state',
                state: {
                    component: 'select',
                    label: 'State',
                    flex: 1,
                    style: {
                        width: 'fit-content'
                    },
                    settings: {
                        optionfn: {
                            funcName: 'getStates',
                            dependentFields: []
                        }
                    }
                },
                city: {
                    component: 'select',
                    label: 'City',
                    flex: 1,
                    settings: {
                        optionfn: {
                            funcName: 'getCities',
                            dependentFields: ['location.state']
                        }
                    }
                }
            }
        },
        wrapperButtons: {
            className: 'wrapperButtons',
            signup: {
                component: 'greenButton',
                flex: 1,
                justify: 'end',
                settings: {
                    text: 'SIGN-UP',
                    onClickFn: 'showData',
                }
            },
            back: {
                component: 'button',
                flex: 1,
                style: {
                    textAlign: 'end'
                },
                settings: {
                    text: 'Back',
                    type: 'default'
                    // onClickFn: '',
                }
            },
        }
    }
};
