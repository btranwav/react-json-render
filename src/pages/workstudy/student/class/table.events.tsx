import { message } from "antd";
import { FormikContextType } from "formik";

const importColumns = [
  {
    title: "Name",
    dataIndex: "name",
  },
  {
    title: "Age",
    dataIndex: "age",
  },
  {
    title: "Address",
    dataIndex: "address",
  },
];

function loadCleverClasses() {
  return (page: number, pageSize: number) => {
    const data: any[] = [];
    const start = page * pageSize + 1;
    const end = start + pageSize;
    for (let i = start; i < end; i++) {
      data.push({
        key: i,
        name: `Edward King ${i}`,
        age: 32,
        address: `London, Park Lane no. ${i}`,
      });
    }

    return Promise.resolve([500, data]);
  };
}

function getPaginationOptions() {
  return () => ({
    position: ["rightBottom"],
    pageSizeOptions: [5, 10, 15],
    pageSize: 15,
    // onChange: (page: number, pageSize: number) => loadCleverClasses()(page, pageSize)
  });
}

function onRowSelected(formikContext: FormikContextType<any>) {
  return (selectedRowKeys: React.Key[], selectedRows: any[]) => {
    message.info("New row selected");
  };
}

const TableEvents = {
  importColumns,
  loadCleverClasses,
  getPaginationOptions,
  onRowSelected,
};

export default TableEvents;
