import { FormikContextType } from "formik";

function importClever(formikContext: FormikContextType<any>) {
    return () => {
        formikContext.setFieldValue('showImportClever', 1, false);
    }
}

function googleSignup() {
    return () => {

    }
}

function classLinkSignup() {
    return () => {

    }
}

function manualSignup() {
    return () => {

    }
}

function importClasses() {
    return () => {

    }
}

function closeImportModal(formikContext: FormikContextType<any>) {
    return () => {
        formikContext.setFieldValue('showImportClever', 0, false);
    }
}

const import_classes = {
    classLinkSignup,
    importClever,
    googleSignup,
    manualSignup,
    closeImportModal,
    importClasses
};

export default import_classes;