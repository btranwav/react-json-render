import TableEvents from "./table.events";

export const import_clever_modal_schema = {
    uniqueName: 'import_clever_modal_schema',
    validationSchema: null,
    initialValue: {},
    formSchema: {
        modalTitle: {
            flex: 1,
            component: 'title',
            value: 'Import classes from Clever Classroom'
        },
        importTable: {
            component: 'table',
            settings: {
                columns: TableEvents.importColumns,
                loadDataFn: TableEvents.loadCleverClasses.name,
                paginationFn: TableEvents.getPaginationOptions.name,
                onRowSelectedFn: TableEvents.onRowSelected.name
            }
        }
    }
};
