import { Op } from "../../../../dynamic-render/types/operator";
import { import_clever_modal_schema } from "./import-clever-modal.schema";

export const add_students_screen = {
  uniqueName: "add_students_screen",
  validationSchema: null,
  initialValue: { showImportClever: 0 },
  formSchema: {
    pageTitle: {
      component: "title",
      value: "Add students to your class",
    },
    hr: {
      component: "hr",
    },
    addStudentTabs: {
      tabs: [
        {
          tabName: "Add manually",
          group: {
            className: "vertical_form",
            instruction: {
              component: "label",
              settings: {
                text: "Enter student names below:",
              },
            },
          },
        },
        {
          tabName: "Import",
          group: {
            className: "vertical_form",
            style: {
              //   display: "flex",
              //   flexDirection: "column",
              gap: "15px",
            },
            clever: {
              component: "signupButton",
              justify: 'center',
              settings: {
                text: "Import from Clever",
                imgSource:
                  "https://media.glassdoor.com/sqll/766297/clever-squarelogo-1571854281108.png",
                onClickFn: "importClever",
                className: "signup_button",
              },
            },
            google: {
              component: "signupButton",
              justify: 'center',
              settings: {
                text: "Import from Google Classroom",
                imgSource:
                  "https://upload.wikimedia.org/wikipedia/commons/5/59/Google_Classroom_Logo.png",
                onClickFn: "googleSignup",
                className: "signup_button",
              },
            },
            classLink: {
              component: "signupButton",
              justify: 'center',
              settings: {
                text: "Import from excel",
                imgSource:
                  "https://cdn2.downdetector.com/static/uploads/logo/classlink-log.png",
                onClickFn: "classLinkSignup",
                className: "signup_button",
              },
            },
          },
        },
      ],
    },
    importCleverModal: {
      component: "modal",
      title: "Import classes from Clever Classroom",
      formConfig: import_clever_modal_schema,
      onOkFn: "importClasses",
      onCancelFn: "closeImportModal",
      okText: "Import classes",
      hiddenIf: {
        field: "showImportClever",
        op: Op.equals,
        value: 0,
      },
    },
  },
};
