import { FormikContextType } from "formik";

function submitClass(formikContext: FormikContextType<any>) {
  return () => {
    formikContext.validateForm().then((result: any) => {
      formikContext.setTouched(result);
      alert(JSON.stringify(formikContext.values));
    });
  };
}

const setup_class_events = {
  submitClass,
};

export default setup_class_events;
