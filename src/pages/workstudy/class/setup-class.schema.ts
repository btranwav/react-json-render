import yup from "../../../dynamic-render/registries/validator.registry";

export const setup_class_screen = {
    uniqueName: 'setup_class_screen',
    validationSchema: yup.object().shape({
        class: yup.string().required(),
        grades: yup.array().of(yup.string()).min(2)
    }),
    initialValue: {},
    formSchema: {
        style: {
            width: '50%'
        },
        pageTitle: {
            component: 'title',
            value: 'Setup your first Classroom'
        },
        hr: {
            component: 'hr'
        },
        wrapperForm: {
            className: 'vertical_form',
            class: {
                component: 'worky_text',
                label: 'Give your class a name',
                flex: 0,
                settings: {
                    placeholder: 'Class Name, e.g Class 4B',
                }
            },
            grades: {
                component: 'grades',
                settings: {
                    text: "Select grade level",
                    tags: ['PreK', 'K', '1', '2', '3', '4', '5', '6', '7', '8', 'Other']
                }
            }
        },
        wrapperButtons: {
            className: 'wrapperButtons',
            back: {
                component: 'button',
                flex: 1,
                justify: 'end',
                settings: {
                    text: 'I will do this later',
                    type: 'default'
                    // onClickFn: '',
                }
            },
            signup: {
                component: 'greenButton',
                flex: 1,
                style: {
                    textAlign: 'end'
                },
                settings: {
                    text: 'CREATE CLASS',
                    onClickFn: 'submitClass',
                }
            },
        }
    }
};
