import { Input } from "antd";
import React from "react";

export function WorkyTextbox({ disabled, settings, ...rest }: any) {
  const { className, ...restSettings } = settings;
  return (
    <Input className={`worky_text ${settings.className}`}
      // value={value}
      disabled={disabled}
      // onChange={(evt) => onChange(evt.target.value)}
      {...rest}
      {...restSettings}
    />
  );
}
