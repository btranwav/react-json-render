import { set } from "lodash";
import { SimpleButton } from "../../../dynamic-render/components/Button"

export const GreenButton = (props: any) => {
    set(props, 'settings.style.backgroundColor', 'green');
    set(props, 'settings.style.borderColor', 'green');
    return (
        <SimpleButton {...props} />)
}