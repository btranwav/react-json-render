import { Button, Image, Space } from "antd"
import { useFormikContext } from "formik";
import { getEventHandler } from "../../../dynamic-render/registries/event.registry";
import '../style.scss';

const SignUpButton = ({ settings }: any) => {
    const { text, imgSource, onClickFn, ...rest } = settings;
    const formikContext = useFormikContext();
    const handler = getEventHandler(onClickFn, formikContext);

    return (
        <Space size={1} onClick={handler} {...rest}>
            <Image
                width={50}
                height={50}
                style={{
                    border: 'solid 1px cyan',
                    padding: '10px',
                    display: 'inherit'
                }}
                preview={false}
                src={imgSource} />
            <Button style={{
                textAlign: 'left',
                backgroundColor: 'darkblue',
                border: 'solid 1px darkblue',
                color: 'white',
                width: '300px',
                height: '50px',
                borderRadius: 0
            }}>{text}</Button>
        </Space>
    );
};

export default SignUpButton;