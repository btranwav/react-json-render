import { Tag } from "antd";
import React, { useMemo, useState } from "react";
import { useFormikContext } from "formik";

const { CheckableTag } = Tag;

const Grades = ({ value, name, onBlur, settings }: any) => {
  const { text, tags = [], ...rest } = settings;

  const [selectedTags, setSelectedTags] = useState<string[]>(value || []);

  const formikContext = useFormikContext();

  const handleChange = (tag: string, checked: boolean) => {
    const nextSelectedTags = checked
      ? [...selectedTags, tag]
      : selectedTags.filter((t) => t !== tag);
    console.log("You are interested in: ", nextSelectedTags);
    setSelectedTags(nextSelectedTags);
    formikContext.setFieldTouched(name, true);
    formikContext.setFieldValue(name, nextSelectedTags, true);
  };

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const memoChange = useMemo(() => handleChange, [selectedTags]);

  return (
    <div style={{ textAlign: "left" }} className="grades__button" {...rest}>
      <div style={{ marginRight: 8 }}>{text}</div>
      <div>
        {tags.map((tag: string) => (
          <CheckableTag
            style={{
              border: "solid 1px lightgray",
              borderRadius: 10,
              minWidth: 34,
              textAlign: "center",
            }}
            key={tag}
            checked={selectedTags.indexOf(tag) > -1}
            onChange={(checked) => memoChange(tag, checked)}
          >
            {tag}
          </CheckableTag>
        ))}
      </div>
    </div>
  );
};

export default Grades;
