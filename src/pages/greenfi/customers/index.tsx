import { useEffect, useState } from "react";
import DynamicForm from "../../../dynamic-render/core/DynamicForm";
import useLoading from "../../../dynamic-render/hooks/useLoading";
import { registerEvents } from "../../../dynamic-render/registries/event.registry";
import { validateData } from "../../../dynamic-render/validate-data";
import events from "./events";
import './styles.scss';
import schema from "./schema";
import TableActions from './table-actions';
import RenderActions from './table.render';

const CustomersComponent = () => {
    registerEvents(events);
    registerEvents(TableActions);
    registerEvents(RenderActions);

    const [formSchema, setFormSchema] = useState({});
    const [validationSchema, setValidationSchema] = useState(null);
    const formConfig = useLoading(schema, false);
    const [initialValue, setInitialValue] = useState();

    useEffect(() => {
        registerEvents(events);

        let temp = formConfig.initialValue;
        validateData(formConfig.formSchema, temp)
        setInitialValue(temp);
        setValidationSchema(formConfig.validationSchema);
        setFormSchema(formConfig.formSchema);
        // eslint-disable-next-line
    }, [schema])

    return (
        <DynamicForm
            id={schema.uniqueName}
            formSchema={formSchema}
            initialValue={initialValue}
            validationSchema={validationSchema}
            // layout='horizontal'
        />
    );
}

export default CustomersComponent;