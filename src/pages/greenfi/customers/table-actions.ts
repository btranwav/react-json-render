import { message } from "antd";
import { FormikContextType } from "formik";

const customerColumns = 
  [
    {
      title: "No",
      dataIndex: "no",
    },
    {
      title: "Company Name",
      dataIndex: "companyName",
    },
    {
      title: "Address",
      dataIndex: "address",
    },
    {
      title: "Postal Code",
      dataIndex: "postalCode",
    },
    {
      title: "Country",
      dataIndex: "country",
    },
    {
      title: "Industry",
      dataIndex: "industry",
    },
    {
      title: "Revenue",
      dataIndex: "revenue",
    },
    {
      title: "Actions",
      dataIndex: "actions",
      renderFn: 'renderCustomerActions',
    },
    {
      title: "Assets",
      dataIndex: "assets",
      renderFn: 'renderAssetAction'
    },
  ];

function loadCustomers() {
  return (page: number, pageSize: number) => {
    const data: any[] = [];
    const start = page * pageSize + 1;
    const end = start + pageSize;
    for (let i = start; i < end; i++) {
      data.push({
        key: i,
        no: i,
        companyName: `Nike Singapre PTE LTD ${i}`,
        age: 32,
        address: `London, Park Lane no. ${i}`,
        postalCode: 117440 + i,
        country: 'Singapore',
        industry: 'Retail',
        revenue: '$5M',
      });
    }

    return Promise.resolve([500, data]);
  };
}

function getPaginationOptions() {
  return () => ({
    position: ["rightBottom"],
    // pageSizeOptions: [5, 10, 15],
    pageSize: 10,
    // onChange: (page: number, pageSize: number) => loadCleverClasses()(page, pageSize)
  });
}

function onRowSelected(formikContext: FormikContextType<any>) {
  return (selectedRowKeys: React.Key[], selectedRows: any[]) => {
    message.info("New row selected");
  };
}

const TableActions = {
  customerColumns,
  loadCustomers,
  getPaginationOptions,
  onRowSelected,
};

export default TableActions;
