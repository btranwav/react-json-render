import { FormikContextType } from "formik";
import { NavigateFunction } from "react-router-dom";

function addAsset(formikContext: FormikContextType<any>, navigate: NavigateFunction) {
  return () => {
    navigate("/add-asset");
  };
}

const events = { addAsset };

export default events;
