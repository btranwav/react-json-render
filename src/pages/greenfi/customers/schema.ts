import { ICON } from "../../../dynamic-render/registries/icon.registry";
import yup from "../../../dynamic-render/registries/validator.registry";
import TableActions from "./table-actions";
import { keyword_config } from "../common/keyword-search";

const customers_screen = {
  uniqueName: "customers_screen",
  validationSchema: yup.object({}),
  initialValue: {
    title: "Corporate Customers",
  },
  formSchema: {
    title: {
      component: "title",
    },
    wrapperSearch: {
      justify: "space-between",
      keyword: keyword_config,
      wrapperFilter: {
        flex: 1,
        country: {
          flex: 1,
          component: "select",
          settings: {
            options: [
              {
                label: "Country",
                value: "",
              },
              {
                label: "Singapore",
                value: "Singapore",
              },
            ],
          },
        },
        industry: {
          flex: 1,
          component: "select",
          settings: {
            options: [
              {
                label: "Industry",
                value: "",
              },
              {
                label: "Retail",
                value: "Retail",
              },
            ],
          },
        },
        addNewBtn: {
          style: {
            width: "fit-content",
          },
          component: "iconButton",
          settings: {
            text: "Add new",
            icon: ICON.PlusCircleOutlined,
            onClickFn: "addAsset",
          },
        },
      },
    },
    customerTable: {
      component: "table",
      settings: {
        columns: TableActions.customerColumns,
        loadDataFn: TableActions.loadCustomers.name,
        paginationFn: TableActions.getPaginationOptions.name,
        onRowSelectedFn: "",
      },
    },
  },
};

export default customers_screen;
