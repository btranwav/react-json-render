import React from "react";
import { Space } from "antd";
import { FormikContextType } from "formik";
import {
  EnvironmentOutlined,
  QuestionCircleOutlined,
  EyeOutlined,
} from "@ant-design/icons";
import { Link } from "react-router-dom";

const renderCustomerActions = (
  formikContext: FormikContextType<any>
) => {
  return (_: any, row: any) => (
    <Space size="middle">
      <a href="_blank">
        <EnvironmentOutlined rev={undefined} />
      </a>
      <a href="_blank">
        <QuestionCircleOutlined rev={undefined} />
      </a>
    </Space>
  );
};

const renderAssetAction = (formikContext: FormikContextType<any>) => {
  return (_: any, row: any) => (
    <Link to="/assets"><EyeOutlined rev={undefined} /></Link>
  );
};

const renderActions = {
  renderCustomerActions,
  renderAssetAction
};

export default renderActions;
