import { ICON } from "../../../dynamic-render/registries/icon.registry";
import yup from "../../../dynamic-render/registries/validator.registry";
import { Op } from "../../../dynamic-render/types/operator";
import { add_user_modal_schema } from "./add-user-modal.schema";
import events from "./events";
import TableActions from "./table-actions";

const user_management_screen = {
  uniqueName: "user_management_screen",
  validationSchema: yup.object({}),
  initialValue: {
    title: "User Management",
    showUserModal: 0,
  },
  formSchema: {
    wrapperTitle: {
      title: {
        flex: 1,
        component: "title",
      },
      exportBtn: {
        style: {
          width: "fit-content",
          alignContent: "center",
        },
        component: "iconButton",
        settings: {
          text: "Export data",
          icon: ICON.ExportOutlined,
        },
      },
    },
    wrapperSearch: {
      justify: "space-between",
      keyword: {
        flex: 1,
        component: "text",
        settings: {
          style: {
            width: 200,
          },
          placeholder: "Search",
          prefix: "SearchOutlined",
          type: "",
        },
      },
      wrapperFilter: {
        style: {
          width: "fit-content",
        },
        addNewBtn: {
          style: {
            width: "fit-content",
          },
          component: "iconButton",
          settings: {
            text: "Add User",
            icon: ICON.PlusCircleOutlined,
            onClickFn: events.onClickAddBtn.name
          },
        },
        uploadUsers: {
          component: "iconButton",
          style: {
            width: "fit-content",
          },
          settings: {
            text: "Upload User List",
            icon: ICON.UploadOutlined,
            type: "default",
          },
        },
      },
      customerTable: {
        component: "table",
        settings: {
          columns: TableActions.userColumns,
          loadDataFn: TableActions.loadUsers.name,
          paginationFn: TableActions.getPaginationOptions.name,
          onRowSelectedFn: "",
        },
      },
    },
    addUserModal: {
      component: "modal",
      title: "Add new user",
      formConfig: add_user_modal_schema,
      onOkFn: events.addUser.name,
      onCancelFn: events.closeModal.name,
      width: '500px',
      okText: "Add new",
      closeIf: [{
        field: "showUserModal",
        op: Op.notEquals,
        value: 1,
      }],
    },
  },
};

export default user_management_screen;
