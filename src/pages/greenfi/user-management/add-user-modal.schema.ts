
export const add_user_modal_schema = {
    uniqueName: 'add_user_modal_schema',
    validationSchema: null,
    initialValue: {
    },
    formSchema: {
        modalTitle: {
            flex: 1,
            component: 'title',
            value: 'Add new user'
        },
        firstName: {
            component: 'text',
            label: 'First name'
        },
        lastName: {
            component: 'text',
            label: 'Last name'
        },
        username: {
            component: 'text',
            label: 'Username'
        },
        department: {
            component: 'text',
            label: 'Department'
        },
        email: {
            component: 'text',
            label: 'Email'
        },
        // addButton: {
        //     component: 'button',
        //     settings: {
        //         text: 'Add new'
        //     }
        // }
    }
};
