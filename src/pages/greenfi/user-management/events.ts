import { FormikContextType } from "formik";

function addUser(formikContext: FormikContextType<any>) {
    return () => {
        formikContext.setFieldValue('showUserModal', 0);
    };
}

function closeModal(formikContext: FormikContextType<any>) {
    return () => {
        formikContext.setFieldValue('showUserModal', 0);
    };
}

function onClickAddBtn(formikContext: FormikContextType<any>) {
    return () => {
        formikContext.setFieldValue('showUserModal', 1);
    }
}

const events = {
    addUser,
    closeModal,
    onClickAddBtn
};

export default events;