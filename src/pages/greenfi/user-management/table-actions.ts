import { message } from "antd";
import { FormikContextType } from "formik";
import renderActions from "./table.render";

const userColumns = 
  [
    {
      title: "No",
      dataIndex: "no",
    },
    {
      title: "First Name",
      dataIndex: "firstName",
    },
    {
      title: "Last Name",
      dataIndex: "lastName",
    },
    {
      title: "Username",
      dataIndex: "username",
    },
    {
      title: "Department",
      dataIndex: "department",
    },
    {
      title: "Office email id",
      dataIndex: "email",
    },
    {
      title: "Status",
      dataIndex: "status",
    },
    {
      title: "Action",
      dataIndex: "action",
      renderFn: renderActions.renderTableActions,
    },
  ];

function loadUsers() {
  return (page: number, pageSize: number) => {
    const data: any[] = [];
    const start = page * pageSize + 1;
    const end = start + pageSize;
    for (let i = start; i < end; i++) {
      data.push({
        key: i,
        no: i,
        firstName: `Thanh`,
        lastName: 'Le',
        department: `Marketing`,
        email: 'thanh.le@gmail.com',
        status: i % 2 === 0 ? 'Active' : 'Inactive'
      });
    }

    return Promise.resolve([500, data]);
  };
}

function getPaginationOptions() {
  return () => ({
    position: ["rightBottom"],
    // pageSizeOptions: [5, 10, 15],
    pageSize: 10,
    // onChange: (page: number, pageSize: number) => loadCleverClasses()(page, pageSize)
  });
}

function onRowSelected(formikContext: FormikContextType<any>) {
  return (selectedRowKeys: React.Key[], selectedRows: any[]) => {
    message.info("New row selected");
  };
}

const TableActions = {
  userColumns,
  loadUsers,
  getPaginationOptions,
  onRowSelected,
};

export default TableActions;
