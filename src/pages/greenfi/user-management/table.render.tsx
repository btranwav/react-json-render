import React from "react";
import { Button, Space } from "antd";
import { FormikContextType } from "formik";
import { DeleteOutlined } from "@ant-design/icons";

const renderTableActions = (formikContext: FormikContextType<any>) => {
  return (_: any, row: any) => (
    <Space size="middle">
      <Button type="primary">Approve</Button>
      <Button type="default" danger={true}>
        Reject
      </Button>
      <DeleteOutlined rev={undefined} />
    </Space>
  );
};

const renderActions = {
  renderTableActions,
};

export default renderActions;
