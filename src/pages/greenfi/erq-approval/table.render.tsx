import React from "react";
import { Button, Space } from "antd";
import { FormikContextType } from "formik";
import {
  FlagOutlined
} from "@ant-design/icons";

const renderErqAction = (
  formikContext: FormikContextType<any>
) => {
  return (_: any, row: any) => (
    <Space size="middle">
      <Button type='primary' onClick={() => {
        window.location.href = '/review-questionaire'
      }}>Review</Button>
    </Space>
  );
};

const renderFlagAction = (formikContext: FormikContextType<any>) => {
  return (_: any, row: any) => (
    <FlagOutlined rev={undefined} />
  );
};

const renderActions = {
  renderErqAction,
  renderFlagAction
};

export default renderActions;
