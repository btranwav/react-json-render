export interface Questionaire {
  question: {
    text: string;
    answer: {
      text: string;
      attachment?: string;
    };
    question?: Questionaire;
  };
}

export const sample: Questionaire = [
  {
    question: {
      text: "Has the customer faced or expected to face any impact from legal risk?",
      answer: {
        text: "Yes - High Risk",
        attachment: null,
      },
    },
  },
  {
    question: {
      text: "Has the customer obtained any external rating for environment performance?",
      answer: {
        text: "",
        attachment: null,
      },
      question: {
        text: "If yes, please share rating details; and please provide rating report.",
        answer: {
          text: "",
          attachment: "",
        },
      },
    },
  },
];
