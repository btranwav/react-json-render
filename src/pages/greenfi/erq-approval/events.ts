import { FormikContextType } from "formik";
import { NavigateFunction } from "react-router-dom";

function review(formikContext: FormikContextType<any>, navigate: NavigateFunction) {
  return () => {
    navigate("/review");
  };
}

function flag() {
  return () => {};
}

const events = { review, flag };

export default events;
