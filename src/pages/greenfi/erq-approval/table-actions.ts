import { FormikContextType } from "formik";
import renderActions from './table.render';
import { MessageInstance } from "antd/es/message/interface";

const erqColumns = 
  [
    {
      title: "No",
      dataIndex: "no",
    },
    {
      title: "Company Name",
      dataIndex: "companyName",
    },
    {
      title: "Country",
      dataIndex: "country",
    },
    {
      title: "Industry",
      dataIndex: "industry",
    },
    {
      title: "Flag",
      dataIndex: "flag",
      renderFn: renderActions.renderFlagAction.name,
    },
    {
      title: "Actions",
      dataIndex: "actions",
      renderFn: renderActions.renderErqAction.name,
    }
  ];

function loadErq() {
  return (page: number, pageSize: number) => {
    const data: any[] = [];
    const start = page * pageSize + 1;
    const end = start + pageSize;
    for (let i = start; i < end; i++) {
      data.push({
        key: i,
        no: i,
        companyName: `Nike Singapre PTE LTD ${i}`,
        country: 'Singapore',
        industry: 'Apparel',
      });
    }

    return Promise.resolve([500, data]);
  };
}

function getPaginationOptions() {
  return () => ({
    position: ["rightBottom"],
    // pageSizeOptions: [5, 10, 15],
    pageSize: 10,
    // onChange: (page: number, pageSize: number) => loadCleverClasses()(page, pageSize)
  });
}

function onRowSelected(formikContext: FormikContextType<any>, {messageApi}: {messageApi: MessageInstance}) {
  return (selectedRowKeys: React.Key[], selectedRows: any[]) => {
    messageApi.info("New row selected");
  };
}

const TableActions = {
  erqColumns,
  loadErq,
  getPaginationOptions,
  onRowSelected,
};

export default TableActions;
