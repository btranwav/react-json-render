import yup from "../../../dynamic-render/registries/validator.registry";
import TableActions from "./table-actions";
import { keyword_config } from "../common/keyword-search";

const erq_approval_screen = {
  uniqueName: "erq_approval_screen",
  validationSchema: yup.object({}),
  initialValue: {
    title: "ERQ Approval",
  },
  formSchema: {
    title: {
      component: "title",
    },
    wrapperSearch: {
      justify: "space-between",
      keyword: keyword_config,
      wrapperFilter: {
        flex: 1,
        country: {
          flex: 1,
          component: "select",
          settings: {
            options: [
              {
                label: "Country",
                value: "",
              },
              {
                label: "Singapore",
                value: "Singapore",
              },
            ],
          },
        },
        industry: {
          flex: 1,
          component: "select",
          settings: {
            options: [
              {
                label: "Industry",
                value: "",
              },
              {
                label: "Retail",
                value: "Retail",
              },
            ],
          },
        },
        addNewBtn: {
          style: {
            width: "fit-content",
          },
          component: "iconButton",
          settings: {
            text: "Flag",
            onClickFn: "flag",
          },
        },
      },
    },
    customerTable: {
      component: "table",
      settings: {
        columns: TableActions.erqColumns,
        loadDataFn: TableActions.loadErq.name,
        paginationFn: TableActions.getPaginationOptions.name,
        onRowSelectedFn: TableActions.onRowSelected.name,
      },
    },
  },
};

export default erq_approval_screen;
