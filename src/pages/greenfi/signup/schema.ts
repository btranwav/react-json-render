import yup from "../../../dynamic-render/registries/validator.registry";

const signup_screen = {
  uniqueName: "signup_screen",
  validationSchema: yup.object({
    firstName: yup.string().required(),
    lastName: yup.string().required()
  }),
  initialValue: {
    title: "Sign Up",
  },
  formSchema: {
    wrapperRight: {
      style: {
        width: "300px",
      },
      title: {
        justify: "center",
        component: "title",
      },
      wrapperName: {
        justify: "space-between",
        firstName: {
          flex: 1,
          component: "text",
          settings: {
            placeholder: "First name",
          },
        },
        lastName: {
          flex: 1,
          component: "text",
          settings: {
            placeholder: "Last name",
          },
        },
      },
      department: {
        component: "text",
        settings: {
          placeholder: "Department",
          prefix: 'HomeOutlined'
        },
      },
      email: {
        component: "text",
        settings: {
          placeholder: "Official Email ID",
          prefix: 'MailOutlined'
        },
      },
      userName: {
        component: "text",
        settings: {
          placeholder: "Username",
          prefix: "UserOutlined",
        },
      },
      password: {
        component: "password",
        settings: {
          placeholder: "Password",
          prefix: "LockOutlined",
        },
      },
      button: {
        justify: "center",
        component: "button",
        text: "Sign Up",
        onClickFn: "",
        settings: {
          text: "Sign Up",
        },
      },
      loginLink: {
        component: 'html',
        style: {
          height: 52,
          alignContent: 'center',
        },
        justify: 'center',
        align: 'center',
        html: 'Already have an account? <a href="/greenfi/login">Login</a>'
    }
    },
  },
};

export default signup_screen;
