import { ICON } from "../../../dynamic-render/registries/icon.registry";
import yup from "../../../dynamic-render/registries/validator.registry";

const review_questionaire_screen = {
  uniqueName: "review_questionaire_screen",
  validationSchema: yup.object({}),
  initialValue: {
    title: "NIKE SINGAPORE PTE LTD",
    erqByYear: [
      {
        erqYear: "Nike 2022",
      },
      {
        erqYear: "Nike 2021",
      },
    ],
  },
  formSchema: {
    wrapperHeader: {
      iconButton: {
        style: {
          width: "fit-content",
        },
        flex: "0 0 30px",
        justify: "center",
        component: "icon",
        settings: {
          className: "back-button",
          shape: "circle",
          icon: ICON.ArrowLeftOutlined,
          onClickFn: "backToCustomers",
        },
      },
      wrapperMain: {
        flex: 1,
        wrapperHeader: {
          style: {
            border: "1px solid gray",
            borderRadius: "5px",
          },
          title: {
            component: "title",
            justify: "center",
          },
          address: {
            component: "label",
            justify: "center",
            settings: {
              text: "MAPPLETREE BUSINESS CITY, 30 PASIR PANJANG ROAD, #10-31/22, Postal 177440",
            },
          },
        },
        wrapperBody: {
          style: {
            marginTop: "20px",
            border: "1px solid gray",
            borderRadius: "5px",
          },
          category: {
            component: "select",
            style: {
              width: "fit-content",
            },
            settings: {
              options: [
                {
                  label: "Categories",
                  value: "",
                },
                {
                  label: "Singapore",
                  value: "Singapore",
                },
              ],
            },
          },
          year: {
            component: "select",
            style: {
              width: "fit-content",
            },
            settings: {
              options: [
                {
                  label: "Year",
                  value: "",
                },
                {
                  label: "Retail",
                  value: "Retail",
                },
              ],
            },
          },
        },
      },
      wrapperExport: {
        style: {
          width: "fit-content",
          border: "1px solid gray",
          borderRadius: "5px",
        },
        flex: "0 0 100px",
        erqByYear: [
          {
            erqYear: {
              component: "label",
              settings: {
                text: "",
              },
            },
            downloadBtn: {
              component: "button",
              settings: {
                text: "Download",
              },
            },
          },
        ],
      },
    },
  },
};

export default review_questionaire_screen;
