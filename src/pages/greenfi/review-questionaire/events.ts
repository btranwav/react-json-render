function backToErqApproval() {
  return () => {
    window.history.back();
  };
}

const events = {
  backToErqApproval,
};

export default events;
