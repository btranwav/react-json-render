import { ICON } from "../../../dynamic-render/registries/icon.registry";

export const keyword_config = {
    flex: 1,
    component: "text",
    settings: {
      style: {
        width: 200,
        },
      placeholder: "Search",
      prefix: ICON.SearchOutlined,
    },
};