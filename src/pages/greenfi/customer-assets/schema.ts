import { ICON } from "../../../dynamic-render/registries/icon.registry";
import yup from "../../../dynamic-render/registries/validator.registry";
import TableActions from "./table-actions";

const assets_screen = {
  uniqueName: "assets_screen",
  validationSchema: yup.object({}),
  initialValue: {
    title: "View",
  },
  formSchema: {
    wrapperHeader: {
      style: {
        width: 200
      },
      iconButton: {
        flex: '0 0 30px',
        justify: 'center',
        component: 'icon',
        settings: {
          className: 'back-button',
          shape: 'circle',
          icon: ICON.ArrowLeftOutlined,
          onClickFn: 'backToCustomers'
        }
      },
      title: {
        flex: 1,
        justify: 'center',
        component: "title",
      },
    },
    customerTable: {
      component: "table",
      settings: {
        columns: TableActions.assetColumns,
        loadDataFn: TableActions.loadAssets.name,
        paginationFn: TableActions.getPaginationOptions.name,
        onRowSelectedFn: "",
      },
    },
  },
};

export default assets_screen;
