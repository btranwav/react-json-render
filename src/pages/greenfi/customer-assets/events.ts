function backToCustomers() {
  return () => {
    window.history.back();
  };
}

const events = {
  backToCustomers,
};

export default events;
