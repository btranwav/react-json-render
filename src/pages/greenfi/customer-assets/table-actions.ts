
const assetColumns = 
  [
    {
      title: "SN",
      dataIndex: "sn",
    },
    {
      title: "Asset type",
      dataIndex: "assetType",
    },
    {
      title: "Status",
      dataIndex: "status",
    },
    {
      title: "Criticality",
      dataIndex: "criticality",
    },
    {
      title: "Street",
      dataIndex: "street",
    },
    {
      title: "District",
      dataIndex: "district",
    },
    {
      title: "City",
      dataIndex: "city",
    },
    {
      title: "State",
      dataIndex: "state",
    },
    {
      title: "Country",
      dataIndex: "country",
    },
    {
      title: "Postal Code",
      dataIndex: "postalCode",
    }
  ];

function loadAssets() {
  return (page: number, pageSize: number) => {
    const data: any[] = [];
    const start = page * pageSize + 1;
    const end = start + pageSize;
    for (let i = start; i < end; i++) {
      data.push({
        key: i,
        sn: i,
        assetType: `Nike Singapre PTE LTD ${i}`,
        status: 'Current',
        criticality: `Low`,
        street: `533 + ${i} Changi Road`,
        district: 'East coast',
        city: 'Singapore',
        state: '',
        country: 'Singapore',
        postalCode: 117440 + i,
      });
    }

    return Promise.resolve([500, data]);
  };
}

function getPaginationOptions() {
  return () => ({
    position: ["rightBottom"],
    // pageSizeOptions: [5, 10, 15],
    pageSize: 10,
    // onChange: (page: number, pageSize: number) => loadCleverClasses()(page, pageSize)
  });
}

const TableActions = {
  assetColumns,
  loadAssets,
  getPaginationOptions
};

export default TableActions;
