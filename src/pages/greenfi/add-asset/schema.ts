import yup from "../../../dynamic-render/registries/validator.registry";

const add_asset_screen = {
  uniqueName: "add_asset_screen",
  validationSchema: yup.object({
  }),
  initialValue: {
    title: "Login",
  },
  formSchema: {
    map: {
      component: 'map',
      center: { lat: -25.344, lng: 131.031 },
      zoom: 4
    }
  }
};

export default add_asset_screen;
