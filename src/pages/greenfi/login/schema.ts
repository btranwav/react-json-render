import yup from "../../../dynamic-render/registries/validator.registry";

const login_screen = {
  uniqueName: "login_screen",
  validationSchema: yup.object({
    username: yup.string().required(),
    password: yup.string().required()
  }),
  initialValue: {
    title: "Login",
  },
  formSchema: {
    wrapperRight: {
      style: {
        width: "300px",
      },
      title: {
        justify: "center",
        component: "title",
      },
      userName: {
        component: "text",
        settings: {
          placeholder: "Username",
          prefix: "UserOutlined",
        },
      },
      password: {
        component: "password",
        settings: {
          placeholder: "Password",
          prefix: "LockOutlined",
        },
      },
      wrapperRemember: {
        justify: "space-between",
        rememberMe: {
          flex: 1,
          component: "checkboxes",
          options: [
            {
              label: 'Remember me',
              value: false
            }
          ]
        },
        forgotPassword: {
          flex: 1,
          justify: 'right',
          component: "link",
          settings: {
            text: "Forgot password",
          },
        },
      },
      
      button: {
        justify: "center",
        component: "button",
        text: "Login",
        onClickFn: "",
        settings: {
          text: "Login",
        },
      },
      signupLink: {
          component: 'html',
          style: {
            height: 52,
            alignContent: 'center',
          },
          justify: 'center',
          align: 'center',
          html: 'Don\'t have an account? <a href="/greenfi/signup">Sign up</a>'
      }
    },
  },
};

export default login_screen;
