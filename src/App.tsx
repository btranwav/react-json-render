import React from "react";
import { Route, Routes } from "react-router-dom";
import "./App.css";
import { AuthProvider } from "./dynamic-render/providers/AuthProvider";

import AnonymousLayout from "./AnonymousLayout";
import SignupComponent from "./pages/greenfi/signup";
import ErqComponent from "./pages/greenfi/erq-approval";
import ReviewQuestionaireComponent from "./pages/greenfi/review-questionaire";
import CustomersComponent from "./pages/greenfi/customers";
import AssetsComponent from "./pages/greenfi/customer-assets";
import UserManagementComponent from "./pages/greenfi/user-management";
import AddAssetComponent from "./pages/greenfi/add-asset";
import LoginComponent from "./pages/greenfi/login";
import MainLayout from "./Layout";
import { ConfigProvider } from "antd";
import { greenfi_theme } from "./theme";
import { Status, Wrapper } from "@googlemaps/react-wrapper";
import ConversationAnnotation from "./pages/conversation-annotation/ConversationAnnotation";
import Wysiwyg from "./pages/wysiwyg/Wysiwyg";
import WorkyPage from "./pages/workstudy/WorkyPage";
import { setup_class_screen } from "./pages/workstudy/class/setup-class.schema";
import { signup_screen } from "./pages/workstudy/signup/signup.schema";
import { signup_email_screen } from "./pages/workstudy/signup-with-email/signup-email.schema";
import { add_students_screen } from "./pages/workstudy/student/class/add-student.schema";

const render = (status: Status) => {
  return <h3>{status} ..</h3>;
};

const App = () => {
  return (
    <Wrapper apiKey="AIzaSyAeQrACTknpJoosGd8kKTiWuEmMDh3jGUg" libraries={["places"]} render={render}>
      <ConfigProvider theme={greenfi_theme}>
        <AuthProvider>
          {/* <BrowserRouter> */}
            <Routes>
              <Route path='/e2f/conversation' element={<ConversationAnnotation />} />
              <Route path='/wysiwyg' element={<Wysiwyg />} />
              <Route path="/greenfi" element={<AnonymousLayout />}>
                <Route path="/greenfi/login" element={<LoginComponent />} />
                <Route path="/greenfi/signup" element={<SignupComponent />} />
              </Route>
              <Route path="/" element={<MainLayout />}>
                <Route path="/" element={<CustomersComponent />} />
                <Route path="/erq-approval" element={<ErqComponent />} />
                <Route path="/review-questionaire" element={<ReviewQuestionaireComponent />} />
                <Route path="/customers" element={<CustomersComponent />} />
                <Route path="/assets" element={<AssetsComponent />} />
                <Route path="/users" element={<UserManagementComponent />} />
                <Route path="/add-asset" element={<AddAssetComponent />} />
              </Route>
              <Route path="/worky" element={<MainLayout />}>
                <Route path="/worky/class" element={<WorkyPage schema={setup_class_screen} />} />
                <Route path="/worky/signup" element={<WorkyPage schema={signup_screen} />} />
                <Route path="/worky/signup-email" element={<WorkyPage schema={signup_email_screen} />} />
                <Route path="/worky/student" element={<WorkyPage schema={add_students_screen} />} />
              </Route>
            </Routes>
          {/* </BrowserRouter> */}
        </AuthProvider>
      </ConfigProvider>
    </Wrapper>
  );
};

export default App;
