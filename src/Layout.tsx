import {
  DashboardOutlined,
  HomeOutlined,
  LogoutOutlined,
  QuestionCircleOutlined,
  SnippetsOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { Layout, Menu, MenuProps, Space } from "antd";
import { useState } from "react";
import { Link, Outlet } from "react-router-dom";

type MenuItem = Required<MenuProps>["items"][number];

function getItem(
  label: React.ReactNode,
  key: React.Key,
  icon?: React.ReactNode,
  children?: MenuItem[]
): MenuItem {
  return {
    key,
    icon,
    children,
    label,
  } as MenuItem;
}

const items: MenuItem[] = [
  getItem(<Link to="/"><span>Dashboard</span></Link>, "1", <DashboardOutlined rev={undefined} />),
  getItem(<Link to="/erq-approval"><span>ERQ Approval</span></Link>, "2", <QuestionCircleOutlined rev={undefined} />),
  getItem(<Link to="/customers"><span>Asset Management</span></Link>, "3", <SnippetsOutlined rev={undefined} />),
  getItem(<Link to="/users"><span>User Management</span></Link>, "4", <UserOutlined rev={undefined} />),
  //worky
  getItem(<Link to="/worky/class"><span>Class</span></Link>, "5", <QuestionCircleOutlined rev={undefined} />),
  getItem(<Link to="/worky/signup"><span>Signup</span></Link>, "6", <QuestionCircleOutlined rev={undefined} />),
  getItem(<Link to="/worky/signup-email"><span>Signup Email</span></Link>, "7", <QuestionCircleOutlined rev={undefined} />),
  getItem(<Link to="/worky/student"><span>Student</span></Link>, "8", <QuestionCircleOutlined rev={undefined} />),
  //conversation
  getItem(<Link to="/e2f/conversation"><span>Conversation</span></Link>, "9", <QuestionCircleOutlined rev={undefined} />),
  getItem(<Link to="/wysiwyg"><span>WYSIWYG</span></Link>, "10", <QuestionCircleOutlined rev={undefined} />),
];

export default function MainLayout() {
  const { Header, Content, Footer, Sider } = Layout;
  const [collapsed, setCollapsed] = useState(false);

  return (
    <Layout>
      <Sider
        className="greenfi-slider"
        style={{
          // backgroundColor: "#01401C",
          height: "100vh",
          display: "flex",
          justifyContent: "space-between",
        }}
        collapsible
        collapsed={collapsed}
        trigger={null}
        onCollapse={(value) => setCollapsed(value)}
      >
        <div style={{ display: "flex", flexDirection: "column" }}>
          <div style={{ display: "flex" }}>
            <div>
              <img alt="logo" src="greenfi_logo.jpg" className="greenfi-logo" />
            </div>
            <Space hidden={true}>
              <HomeOutlined rev={undefined} />
              <span>Home</span>
            </Space>
          </div>
          <Menu
            // style={{ backgroundColor: "#01401C" }}
            theme="dark"
            defaultSelectedKeys={["1"]}
            mode="inline"
            items={items}
          />
        </div>
        <div>
          <Menu
            // style={{ backgroundColor: "#01401C" }}
            theme="dark"
            defaultSelectedKeys={["1"]}
            mode="inline"
            items={[getItem("Logout", "4", <LogoutOutlined rev={undefined} />)]}
          />
        </div>
      </Sider>

      <Layout className="layout white-background">
        <Header
          style={{
            display: "flex",
            alignItems: "center",
            backgroundColor: "white",
            justifyContent: "flex-end",
            boxShadow: "0px 13px 10px -5px rgba(124, 128, 118, 0.29)",
          }}
        >
          <div>User Info</div>
        </Header>
        <Content className="site-layout-content">
          {/* <div style={{ display: "flex", flexDirection: "column", width: "100%" }}> */}
            <Outlet />
          {/* </div> */}
        </Content>
        <Footer hidden={true} className="greenfi-footer">
          Ant Design ©2023 Created by Ant UED
        </Footer>
      </Layout>
    </Layout>
  );
}
