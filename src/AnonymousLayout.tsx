import { Outlet } from "react-router-dom";

export default function AnonymousLayout() {
  return (
    <div className="greenfi-anonymous-layout">
      <div className="greenfi-background"></div>
      <div style={{ flex: 1 }}>
        <Outlet />
      </div>
    </div>
  );
}
